This project consists of two parts:
the frontend application and the backend API.
The frontend application is stored in `/react-project`,
and the backend API project is stored in the project root.

### Running the Frontend Project

To develop the frontend, you can make use of the `webpack-dev-server` package 
which is bundled with the project.
Navigate to `/react-project`,
and run the following commands:
```shell script
npm install
npm run compile
npm run dev
```

The final command will begin a live-reloading application,
which launches in your browser,
and reloads whenever there are changes to the source files.
You can force a reload by running `npm run compile` again
in a separate shell window.

The `webpack-dev-server` application hosts a simple HTML file
at `localhost:9001`.

**Note**: the `webpack-dev-server` does not expose the API endpoints
that are supplied by the backend project.
It is only suitable for development which does not use the API.

### Running the Backend Project

Whether you wish to work on the backend project,
or want to work on frontend communication,
you'll need to be able to run the backend project.
The project is written in Kotlin and uses Ktor+PostgreSQL.

First, either launch a PostgreSQL instance locally,
or if this not an option,
you can ask @Llamos, a project maintainer,
for access to a PostgreSQL schema.

Within `/src/main/resources/`, there exists `application.conf`,
which contains application settings, loaded on application start.
Within the same folder you can create `development.conf`,
which will be automatically included at startup,
and override any values within `application.conf`.

Within `development.conf` you will need to provide database credentials.
You can do so with the following snippet:
```hocon
db {
  url = "jdbc:postgresql://POSTGRES_HOST/POSTGRES_DATABASE?currentSchema=POSTGRES_SCHEMA"
  user = "POSTGRES_USERNAME"
  password = "POSTGRES_PASSWORD"
  hikari = false // <-- recommended, disables connection pooling
}
``` 

Once this is set up, build the npm project with `npm run build` in the `/react-project/` folder.
For more info, see "Running the Frontend Project" above.

Finally, you can launch the web server either through your IDE,
or via `./gradlew run` on Mac/Linux, or `gradlew.bat run` on Windows.
The application will launch binding to `0.0.0.0:9000`.
You can then access the program in your browser using `http://localhost:9000`, 
or any other applicable address for your host.
This default port can be changed via the `PORT` environment variable,
or the `ktor.deployment.port` setting in `/src/main/resources/development.conf`.

### Pipelines

Each push to the repository will trigger an automatic development pipeline.
Throughout the pipeline, the application will be tested, compiled, 
and packaged into a Docker image specified in `/Dockerfile`.

If the commit is **not** on the `master` branch,
then the pipeline will also include a `Staging` stage.
This stage does not run automatically,
and is not required to ensure a pass in the pipeline.
If run, it will create a staging environment automatically,
and launch the web service on a dedicated host.
Further staging deployments to the same branch will overwrite previous deployments.
Since pipeline runtime is extensive, I recommend not using these staging environments for testing,
but rather for demoing or integration testing your changes after they have been tested locally.

If the commit is on the `master` branch
the last phase will instead be `Deploy`.
This task will automatically deploy the packaged application into the production environment.
For this reason, the `master` branch is write-protected and outside changes will require a merge request.
