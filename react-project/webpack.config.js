const path = require("path");
const webpack = require("webpack");
const HtmlWebpackPlugin = require("html-webpack-plugin");
require("babel-polyfill");

const ASSET_PATH = process.env.ASSET_PATH || '/';

module.exports = {
    entry: {
        main: ["babel-polyfill", "./src/index.tsx"]
    },
    mode: "development",
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.ts(x?)$/,
                exclude: /node_modules/,
                use: ["ts-loader"]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                loader: "source-map-loader"
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                  use: [
                    'file-loader',
                  ],
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                  use: [
                    'file-loader',
                  ],
            },
        ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx", ".ts", ".tsx"]
    },
    output: {
        path: path.resolve(__dirname, "../src/main/resources/static/scripts/bundles/"),
        publicPath: ASSET_PATH,
    },
    optimization: {
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 6,
            maxInitialRequests: 4,
            automaticNameDelimiter: '~',
            automaticNameMaxLength: 30,
            cacheGroups: {
                vendors: {
                    chunks: 'initial',
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10,
                    filename: '[name].js',
                    enforce: true
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    filename: '[name].js'
                }
            }
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            template: 'public/index.html',
            favicon: false
        }),
        new webpack.DefinePlugin({
          'process.env.ASSET_PATH': JSON.stringify(ASSET_PATH),
        })
    ],
    devServer: {
        host: 'localhost',
        port: 9001,
        historyApiFallback: true,
        open: true,
        hot: true,
        proxy: {
            '/api': 'http://localhost:9000',
            '/auth': 'http://localhost:9000'
        }
    }
};
