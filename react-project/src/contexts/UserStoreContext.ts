import React from "react";
import userStoreInstance from "../stores/UserStore";

export const UserStoreContext = React.createContext(userStoreInstance);
