const baseRequestInit: RequestInit = {
  cache: "no-cache",
  credentials: "same-origin",
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
  integrity: undefined,
  keepalive: true,
  mode: "same-origin",
  redirect: "error",
  referrerPolicy: "no-referrer",
  signal: null,
};

export type HttpMethod = "GET" | "POST" | "PUT" | "PATCH" | "DELETE";

export interface ApiError {
  messages: string[];
}

export function isApiError<T>(obj: T | ApiError): obj is ApiError {
  return !!(obj as ApiError).messages;
}

export class ApiRequest<TRequest, TResponse> {
  private readonly method: HttpMethod;
  private readonly path: string;
  private readonly body?: TRequest;
  private readonly apiRoot: string;

  constructor(
    method: HttpMethod,
    path: string,
    body: TRequest,
    apiRoot: string = "/api/v1/"
  ) {
    this.method = method;
    this.path = path;
    this.body = body;
    this.apiRoot = apiRoot;
  }

  getMethod(): HttpMethod {
    return this.method;
  }

  getPath(): string {
    return this.path;
  }

  getBody(): TRequest | undefined {
    return this.body;
  }

  getBodyString(): string | undefined {
    return this.body ? JSON.stringify(this.body) : undefined;
  }

  getApiRoot(): string {
    return this.apiRoot;
  }
}

export async function makeRequest<RespType>(
  requestInfo: ApiRequest<unknown, RespType>
): Promise<RespType | ApiError> {
  const response = await doRequest(requestInfo);
  const respBody = await response.json();

  if (!response.ok) return respBody as ApiError;

  return respBody as RespType;
}

async function doRequest(
  request: ApiRequest<unknown, unknown>
): Promise<Response> {
  const req = new Request(request.getApiRoot() + request.getPath(), {
    ...baseRequestInit,
    method: request.getMethod(),
    body: request.getBodyString(),
  });
  return fetch(req);
}
