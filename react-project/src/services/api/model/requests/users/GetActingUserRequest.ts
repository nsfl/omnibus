import { ApiRequest } from "../../../api-call-service";
import { User } from "../../../../../model/rest/User";

export class GetActingUserRequest extends ApiRequest<null, User> {
  constructor() {
    super("GET", "users/me", null);
  }
}
