import * as Api from "../../../api-call-service";
import { User } from "../../../../../model/rest/User";
import ApiRequest = Api.ApiRequest;

export interface CreateUserRequestBody {
  username: string;
  passwordHash: string; // TODO server-side password hashing
  forumUsername: string;
  isAdmin: boolean;
}

export class CreateUserRequest extends ApiRequest<CreateUserRequestBody, User> {
  constructor(body: CreateUserRequestBody) {
    super("POST", "users", body);
  }
}
