import { ApiRequest } from "../../../api-call-service";
import { Player } from "../../../../../model/rest/Player";
import { PlayerPosition } from "../../../../../model/rest/PlayerPosition";

export interface EditPlayerRequestBody {
  draftSeasonId?: number;
  active?: boolean;
  firstName?: string;
  lastName?: string;
  college?: string;
  jerseyNumber?: number;
  age?: number;
  tpeSpent?: number;
  tpeBanked?: number;
  position?: PlayerPosition;
  archetypeId?: number;
  experience?: number;
  height?: number;
  weight?: number;
  strength?: number;
  agility?: number;
  arm?: number;
  speed?: number;
  hands?: number;
  intelligence?: number;
  accuracy?: number;
  passBlocking?: number;
  runBlocking?: number;
  tackling?: number;
  kickDistance?: number;
  kickAccuracy?: number;
  endurance?: number;
  reason?: string;
}

export class EditPlayerRequest extends ApiRequest<
  EditPlayerRequestBody,
  Player
> {
  constructor(playerId: number, body: EditPlayerRequestBody) {
    super("PATCH", `players/${playerId}`, body);
  }
}
