import { PlayerPosition } from "../../../../../model/rest/PlayerPosition";
import { ApiRequest } from "../../../api-call-service";
import { Player } from "../../../../../model/rest/Player";

export interface CreatePlayerRequestBody {
  draftSeasonId: number;
  active: boolean;
  tpeSpent: number;
  tpeBanked: number;
  position: PlayerPosition;
  archetypeId: number;
  firstName: string;
  lastName: string;
  college: string;
  jerseyNumber: number;
  age: number;
  experience: number;
  height: number;
  weight: number;
  strength: number;
  agility: number;
  arm: number;
  speed: number;
  hands: number;
  intelligence: number;
  accuracy: number;
  passBlocking: number;
  runBlocking: number;
  tackling: number;
  kickDistance: number;
  kickAccuracy: number;
  endurance: number;
}

export class CreatePlayerRequest extends ApiRequest<
  CreatePlayerRequestBody,
  Player
> {
  constructor(body: CreatePlayerRequestBody) {
    super("POST", "players", body);
  }
}
