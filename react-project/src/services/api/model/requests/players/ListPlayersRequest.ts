import { ApiRequest } from "../../../api-call-service";
import { Player } from "../../../../../model/rest/Player";

export class ListPlayersRequest extends ApiRequest<null, Player[]> {
  constructor() {
    super("GET", `players`, null);
  }
}
