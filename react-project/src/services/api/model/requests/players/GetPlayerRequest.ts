import { ApiRequest } from "../../../api-call-service";
import { Player } from "../../../../../model/rest/Player";

export class GetPlayerRequest extends ApiRequest<null, Player> {
  constructor(playerId: number) {
    super("GET", `players/${playerId}`, null);
  }
}
