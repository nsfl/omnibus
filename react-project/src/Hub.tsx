import React, { ReactElement, useContext } from "react";
import "./App.css";
import { useObserver } from "mobx-react";
import { UserStoreContext } from "./contexts/UserStoreContext";

const Hub: React.FC = (): ReactElement => {
  const store = useContext(UserStoreContext);

  return useObserver(() => (
    <div>
      <div>Welcome, {store.user && store.user.username}</div>
      <div>
        Your bank account balance is $
        {store.user && store.user.bankAccount && store.user.bankAccount.balance}
      </div>
    </div>
  ));
};

export default Hub;
