import { observable, action } from "mobx";
import { User } from "../model/rest/User";

class UserStore {
  @observable user: User = null;

  @action setUser = (user: User): void => {
    this.user = user;
  };

  @action resetUser = (): void => {
    Object.keys(this.user).forEach((k) => (this.user[k] = null));
  };
}

const userStoreInstance = new UserStore();

export default userStoreInstance;
