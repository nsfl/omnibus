import useRestEffect from "../BaseRestEffect";
import { Player } from "../../../model/rest/Player";
import { Resource } from "../../../util/LoadedResource";
import { GetPlayerRequest } from "../../../services/api/model/requests/players/GetPlayerRequest";

const useSinglePlayerEffect: (playerId: number) => Resource<Player> = (
  playerId: number
) => useRestEffect<null, Player>(new GetPlayerRequest(playerId));

export default useSinglePlayerEffect;
