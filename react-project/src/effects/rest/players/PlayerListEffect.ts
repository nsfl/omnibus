import { ListPlayersRequest } from "../../../services/api/model/requests/players/ListPlayersRequest";
import useRestEffect from "../BaseRestEffect";
import { Player } from "../../../model/rest/Player";
import { Resource } from "../../../util/LoadedResource";

const usePlayerListEffect: () => Resource<Player[]> = () =>
  useRestEffect<null, Player[]>(new ListPlayersRequest());

export default usePlayerListEffect;
