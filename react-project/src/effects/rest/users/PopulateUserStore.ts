import { useEffect, useState, useContext } from "react";
import {
  makeRequest,
  ApiError,
  isApiError,
} from "../../../services/api/api-call-service";
import { GetActingUserRequest } from "../../../services/api/model/requests/users/GetActingUserRequest";
import { User } from "../../../model/rest/User";
import { UserStoreContext } from "../../../contexts/UserStoreContext";

export const usePopulateUserStore = (): boolean => {
  const store = useContext(UserStoreContext);
  const [isFetchingUser, setIsFetchingUser] = useState(true);

  useEffect(() => {
    setIsFetchingUser(true);
    makeRequest(new GetActingUserRequest())
      .then((response: ApiError | User) => {
        if (isApiError(response)) {
          // todo better handling?
          console.warn(
            `Failed to load user data from self-identification endpoint:`,
            response.messages
          );
          store.setUser(null);
          return;
        }

        console.debug("Got self-user info", response);
        store.setUser(response);
        setIsFetchingUser(false);
      })
      .catch((error) => {
        setIsFetchingUser(false);
        console.error(
          "Got unknown error polling self-identification endpoint:",
          error
        );
      });
  }, []);

  return isFetchingUser;
};
