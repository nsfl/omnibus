import { useEffect, useState } from "react";
import { Resource } from "../../util/LoadedResource";
import {
  ApiError,
  ApiRequest,
  isApiError,
  makeRequest,
} from "../../services/api/api-call-service";

export default function useRestEffect<ReqType, RespType>(
  req: ApiRequest<ReqType, RespType>
): Resource<RespType> {
  // set up react stateful prop
  const [resource, setResource] = useState<Resource<RespType>>({
    status: "init",
  });

  // perform background load
  useEffect(() => {
    setResource({ status: "loading" });
    makeRequest(req)
      .then((response: RespType | ApiError) =>
        isApiError(response)
          ? setResource({ status: "error", error: response })
          : setResource({ status: "loaded", payload: response })
      )
      .catch((error) => {
        console.error(error);
        setResource({ status: "error", error: { messages: error.messages } });
      });
  }, []);

  // return new state-tracked prop
  return resource;
}
