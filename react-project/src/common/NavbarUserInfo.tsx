import React, { useContext, ReactElement } from "react";
import { useObserver } from "mobx-react";
import { Button } from "react-bootstrap";
import { UserStoreContext } from "../contexts/UserStoreContext";
import { deleteCookie } from "../util/cookieUtils";
import { useHistory } from "react-router-dom";

interface NavbarUserInfoProps {
  setShow: (show: boolean) => void;
}

const NavbarUserInfo = (props: NavbarUserInfoProps): ReactElement => {
  const history = useHistory();
  const store = useContext(UserStoreContext);
  const handleLogIn = () => props.setShow(true);

  const handleLogOut = async () => {
    await deleteCookie("OmnibusAuthService");
    await store.resetUser();
    history.push("/");
  };

  return useObserver(() =>
    store && store.user && store.user.id ? (
      <Button onClick={() => handleLogOut()}>Log Out</Button>
    ) : (
      <Button onClick={() => handleLogIn()}>Log In</Button>
    )
  );
};

export default NavbarUserInfo;
