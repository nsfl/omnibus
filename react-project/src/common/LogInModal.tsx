import React, { ReactElement } from "react";
import { Modal, Button } from "react-bootstrap";

interface ModalProps {
  show: boolean;
  setShow: (show: boolean) => void;
}
const LogInModal = (props: ModalProps): ReactElement => {
  const handleClose = () => props.setShow(false);

  return (
    <Modal show={props.show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>How to Log In</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          A login link is provided to users privately via a direct message to
          their JCink forum inbox.
        </p>
        <p>You must click that link to authenticate with the omnibus.</p>
        <p>
          If you log out of your account on the omnibus, you must click the
          JCink forum inbox link again to re-authenticate.
        </p>
        <p>
          If you have not received a private message with your account&#39;s log
          in link,{" "}
          <a href="http://forum.sim-football.com/index.php?showtopic=59">
            message a member of Head Office for assistance
          </a>
          .
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={handleClose}>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default LogInModal;
