import React, { ReactElement, useState } from "react";
import { NavLink } from "react-router-dom";
import { LinkContainer } from "react-router-bootstrap";
import Navbar from "react-bootstrap/Navbar";
import leagueLogo from "../assets/league-logo.png";
import Nav from "react-bootstrap/Nav";
import { useObserver } from "mobx-react";
import LogInModal from "./LogInModal";
import NavbarUserInfo from "./NavbarUserInfo";

const ISFLNavbar: React.FC = (): ReactElement => {
  const [show, setShow] = useState(false);

  return useObserver(() => (
    <Navbar bg="dark" variant="dark" expand="lg">
      <Navbar.Brand>
        <NavLink to="/">
          <img
            alt=""
            src={leagueLogo}
            width="30"
            height="30"
            className="d-inline-block align-top"
          />{" "}
          ISFL
        </NavLink>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse>
        <Nav className="ml-auto">
          <LinkContainer to="/players">
            <Nav.Link>Players</Nav.Link>
          </LinkContainer>
          <Nav.Link href="https://nsfl.jcink.net/">Forum</Nav.Link>
          <Nav.Link href="https://index.sim-football.com/">Index</Nav.Link>
          <Nav.Link href="https://tracker.sim-football.com/">Tracker</Nav.Link>
          <NavbarUserInfo setShow={setShow} />
          <LogInModal show={show} setShow={setShow} />
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  ));
};

export default ISFLNavbar;
