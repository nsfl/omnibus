export enum PlayerPosition {
  QB = "QB",
  RB = "RB",
  WR = "WR",
  TE = "TE",
  OL = "OL",
  DE = "DE",
  DT = "DT",
  LB = "LB",
  CB = "CB",
  S = "S",
  KP = "KP",
}
