import { BankAccount } from "./BankAccount";
import { User } from "./User";
import { Season } from "./Season";

export enum BankAccountTransactionStatus {
  PENDING = "PENDING",
  ACCEPTED = "ACCEPTED",
  REJECTED = "REJECTED",
}

export interface BankAccountTransaction {
  id: number;
  recipientId: number;
  recipient?: BankAccount;
  amount: number;
  senderId?: number;
  sender?: BankAccount;
  notes?: string;
  status: BankAccountTransactionStatus;
  responderId?: number;
  responder?: User;
  seasonId: number;
  season?: Season;
}
