export interface BankAccount {
  id: number;
  balance: number;
}
