import { Player } from "./Player";
import { User } from "./User";
import { TPETask } from "./TPETask";

export enum TPEClaimStatus {
  PENDING = "PENDING",
  ACCEPTED = "ACCEPTED",
  REJECTED = "REJECTED",
}

export interface TPEClaim {
  id: number;
  claimantId: number;
  claimant?: Player;
  tpeTaskId?: number;
  tpeTask?: TPETask;
  link: string;
  status: TPEClaimStatus;
  responderId?: number;
  responder?: User;
}
