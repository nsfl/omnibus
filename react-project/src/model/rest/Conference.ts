import { League } from "./League";

export interface Conference {
  id: number;
  name: string;
  acronym: string;
  leagueId: number;
  league?: League;
}
