import { User } from "./User";
import { Archetype } from "./Archetype";
import { PlayerPosition } from "./PlayerPosition";
import { Team } from "./Team";
import { Season } from "./Season";

export interface Player {
  id: number;
  userId: number;
  user?: User;
  draftSeasonId: number;
  draftSeason?: Season;
  retirementSeasonId?: number;
  retirementSeason?: Season;
  active: boolean;
  tpeSpent: number;
  tpeBanked: number;
  position: PlayerPosition;
  archetypeId: number;
  archetype?: Archetype;
  firstName: string;
  lastName: string;
  teamId?: number;
  team?: Team;
  college: string;
  jerseyNumber: number;
  age: number;
  experience: number;
  height: number;
  weight: number;
  strength: number;
  agility: number;
  arm: number;
  speed: number;
  hands: number;
  intelligence: number;
  accuracy: number;
  passBlocking: number;
  runBlocking: number;
  tackling: number;
  kickDistance: number;
  kickAccuracy: number;
  endurance: number;
}
