import { User } from "./User";
import { Season } from "./Season";

export enum TPETaskStatus {
  PENDING = "PENDING",
  OPEN = "OPEN",
  CLOSED = "CLOSED",
  REJECTED = "REJECTED",
}

export interface TPETask {
  id: number;
  creatorId?: number;
  creator?: User;
  name: string;
  description: string;
  link?: string;
  status: TPETaskStatus;
  claimLink: string;
  seasonId: number;
  season?: Season;
}
