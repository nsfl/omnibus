import { Player } from "./Player";
import { User } from "./User";

export interface PlayerHistory {
  id: number;
  playerId: number;
  player?: Player;
  revisionTimestamp: Date;
  changes: string;
  authorId?: number;
  author?: User;
  reason?: string;
}
