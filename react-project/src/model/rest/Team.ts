import { Conference } from "./Conference";
import { BankAccount } from "./BankAccount";
import { User } from "./User";
import { Season } from "./Season";

export interface Team {
  id: number;
  city: string;
  mascot: string;
  acronym: string;
  conferenceId: number;
  conference?: Conference;
  ownerId?: number;
  owner?: User;
  bankAccountId: number;
  bankAccount?: BankAccount;
  startSeasonId: number;
  startSeason?: Season;
  endSeasonId?: number;
  endSeason?: Season;
}
