import { BankAccount } from "./BankAccount";

export interface User {
  id: number;
  username: string;
  passwordHash: string;
  forumUsername: string;
  isAdmin: boolean;
  bankAccount?: BankAccount;
  bankAccountId: number;
}
