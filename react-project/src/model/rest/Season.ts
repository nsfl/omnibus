export interface Season {
  id: number;
  startDate: Date;
  endDate: Date;
  simStartDate: Date;
  simEndDate: Date;
  offSeasonStartDate: Date;
}
