export interface League {
  id: number;
  name: string;
  acronym: string;
}
