import React, { ReactElement } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { useObserver } from "mobx-react";
import "./App.css";
import Home from "./Home";
import ISFLNavbar from "./common/ISFLNavbar";
import Hub from "./Hub";
import { PrivateRoute } from "./util/routeUtils";
import PlayersList from "./views/players/PlayersList";
import PlayerView from "./views/players/PlayerView";
import userStoreInstance from "./stores/UserStore";
import { UserStoreContext } from "./contexts/UserStoreContext";
import { usePopulateUserStore } from "./effects/rest/users/PopulateUserStore";

const App: React.FC = (): ReactElement => {
  const isFetchingUser = usePopulateUserStore();

  return useObserver(() => (
    <UserStoreContext.Provider value={userStoreInstance}>
      <Router>
        {isFetchingUser ? (
          <div>Loading...</div>
        ) : (
          <div className="App">
            <ISFLNavbar />
            <Switch>
              <Route path="/" exact>
                <Home />
              </Route>
              <PrivateRoute path="/hub" exact>
                <Hub />
              </PrivateRoute>
              <Route path="/players" exact>
                <PlayersList />
              </Route>
              <Route path="/players/:id" exact>
                <PlayerView />
              </Route>
            </Switch>
          </div>
        )}
      </Router>
    </UserStoreContext.Provider>
  ));
};

export default App;
