import ReactDOM from "react-dom";
import React from "react";
import App from "./App";
import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/fonts/FiraSans-Regular.woff2";
import "./assets/fonts/FiraSans-Bold.woff2";

ReactDOM.render(<App />, document.getElementById("omnibus-root"));
