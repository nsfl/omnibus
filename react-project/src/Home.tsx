import React, { ReactElement } from "react";
import "./App.css";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from "react-bootstrap/Button";

const Home: React.FC = (): ReactElement => {
  return (
    <div className="homepage">
      <Container className="homepage-container">
        <Row>
          <Col sm={0} md={2} lg={2} />
          <Col sm={12} md={8} lg={6}>
            <h1 className="homepage-header mb-4">
              Become the next gridiron superstar.
            </h1>
            <p className="mb-4">
              The International Simulation Football League is a player-based sim
              league comprised of players created by real people, who have
              complete control of their skills, player type, and career choices.
            </p>
            <Row className="button-container">
              <Col sm={12} md={6} className="mb-4">
                <Button
                  size="lg"
                  href="https://nsfl.jcink.net/"
                  variant="primary"
                  block
                >
                  Sign Up
                </Button>
              </Col>
              <br />
              <Col sm={12} md={6}>
                <Button
                  size="lg"
                  variant="outline-light"
                  href="https://discord.gg/mBqvqtV"
                  className="discord-button"
                  block
                >
                  Join Our Discord
                </Button>
              </Col>
            </Row>
          </Col>
          <Col sm={0} md={2} lg={4} />
        </Row>
      </Container>
    </div>
  );
};

export default Home;
