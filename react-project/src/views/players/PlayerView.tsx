import useSinglePlayerEffect from "../../effects/rest/players/SinglePlayerEffect";
import React from "react";
import { Alert, Col, Container, ProgressBar, Row } from "react-bootstrap";
import { useParams } from "react-router-dom";
import "./PlayerView.css";
import { Player } from "../../model/rest/Player";

interface PlayerStatProps {
  player: Player;
  lookup: string;
  label: string;
}

const PlayerStat: React.FC<PlayerStatProps> = (props: PlayerStatProps) => {
  const capitalized =
    props.lookup.charAt(0).toUpperCase() + props.lookup.slice(1);
  const aMin = props.player.archetype[`base${capitalized}`];
  const aMax = props.player.archetype[`max${capitalized}`];
  const curr = props.player[props.lookup];

  return (
    <Row className="pt-4">
      <Col md={2} xs={4}>
        <strong>
          {props.label}: {curr}/{aMax}
        </strong>
      </Col>
      <Col>
        <ProgressBar>
          <ProgressBar variant="info" now={aMin} />
          <ProgressBar variant="success" now={curr - aMin} />
        </ProgressBar>
      </Col>
    </Row>
  );
};

const PlayerView: React.FC = () => {
  const { id } = useParams();
  const playerId = parseInt(id);
  const player = useSinglePlayerEffect(playerId);

  return (
    <div className="PlayerView">
      {(player.status === "loading" || player.status === "init") && (
        <p>Loading...</p>
      )}
      {player.status === "error" && ( // todo this error handler would probably be nice as its own component
        <Alert variant="danger">
          <Alert.Heading>An Error Occurred</Alert.Heading>
          <ul>
            {player.error.messages.map((error, idx) => (
              <li key={idx}>{error}</li>
            ))}
          </ul>
        </Alert>
      )}
      {player.status === "loaded" && (
        <Container>
          <h2>
            {player.payload.firstName} {player.payload.lastName}
          </h2>
          <span className="text-muted pl-3">
            {player.payload.team ? player.payload.team.acronym : "Free Agent"}{" "}
            {player.payload.position} (S{player.payload.draftSeasonId}
            {player.payload.retirementSeasonId
              ? `-S${player.payload.retirementSeasonId}`
              : ""}
            )
          </span>
          <hr />
          <Row>
            <Col md={3} xs={6}>
              <strong>User</strong>: {player.payload.user.forumUsername}
            </Col>
            <Col md={3} xs={6}>
              <strong>Weight</strong>: {player.payload.weight} lbs.
            </Col>
            <Col md={3} xs={6}>
              <strong>Height</strong>: {player.payload.height} in.
            </Col>
            <Col md={3} xs={6}>
              <strong>Experience</strong>: {player.payload.experience}
            </Col>
          </Row>
          <Row className="pt-4">
            <Col md={3} xs={6}>
              <strong>TPE Spent/Banked</strong>: {player.payload.tpeSpent}/
              {player.payload.tpeBanked}
            </Col>
            <Col md={3} xs={6}>
              <strong>Age</strong>: {player.payload.age}
            </Col>
            <Col md={3} xs={6}>
              <strong>College</strong>: {player.payload.college}
            </Col>
            <Col md={3} xs={6}>
              <strong>Jersey Number</strong>: {player.payload.jerseyNumber}
            </Col>
          </Row>
          <PlayerStat
            label="Strength"
            player={player.payload}
            lookup="strength"
          />
          <PlayerStat
            label="Agility"
            player={player.payload}
            lookup="agility"
          />
          <PlayerStat label="Arm" player={player.payload} lookup="arm" />
          <PlayerStat label="Speed" player={player.payload} lookup="speed" />
          <PlayerStat label="Hands" player={player.payload} lookup="hands" />
          <PlayerStat
            label="Intelligence"
            player={player.payload}
            lookup="intelligence"
          />
          <PlayerStat
            label="Accuracy"
            player={player.payload}
            lookup="accuracy"
          />
          <PlayerStat
            label="PBlocking"
            player={player.payload}
            lookup="passBlocking"
          />
          <PlayerStat
            label="RBlocking"
            player={player.payload}
            lookup="runBlocking"
          />
          <PlayerStat
            label="Tackling"
            player={player.payload}
            lookup="tackling"
          />
          <PlayerStat
            label="KDistance"
            player={player.payload}
            lookup="kickDistance"
          />
          <PlayerStat
            label="KAccuracy"
            player={player.payload}
            lookup="kickAccuracy"
          />
          <PlayerStat
            label="Endurance"
            player={player.payload}
            lookup="endurance"
          />
        </Container>
      )}
    </div>
  );
};

export default PlayerView;
