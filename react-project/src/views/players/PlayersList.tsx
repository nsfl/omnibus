import React from "react";
import { Alert, Table } from "react-bootstrap";
import usePlayerListEffect from "../../effects/rest/players/PlayerListEffect";
import "./PlayersList.css";
import { Link } from "react-router-dom";

const PlayersList: React.FC = () => {
  // stateful info for background request
  const playerList = usePlayerListEffect();

  return (
    <div className="PlayersList m-3">
      <h3>All Players</h3>
      <hr />
      {(playerList.status === "loading" || playerList.status === "init") && (
        <div>Loading....</div>
      )}
      {playerList.status === "error" && (
        <Alert variant={"danger"}>
          <Alert.Heading>An Error Occurred</Alert.Heading>
          <ul>
            {playerList.error.messages.map((error, idx) => (
              <li key={idx}>{error}</li>
            ))}
          </ul>
        </Alert>
      )}
      {playerList.status === "loaded" && (
        <Table responsive hover striped bordered className="m-5">
          <thead className={"thead-dark"}>
            <tr>
              <th scope="col">ID</th>
              <th scope="col">User</th>
              <th scope="col">Draft Year</th>
              <th scope="col">Team</th>
              <th scope="col">Name</th>
              <th scope="col">Position</th>
              <th scope="col">Current TPE</th>
              <th scope="col">Highest TPE</th>
              <th scope="col">Last Updated</th>
              <th scope="col">Last Seen</th>
              <th scope="col">Str</th>
              <th scope="col">Agi</th>
              <th scope="col">Arm</th>
              <th scope="col">Int</th>
              <th scope="col">Thr</th>
              <th scope="col">Tck</th>
              <th scope="col">Spd</th>
              <th scope="col">Hnd</th>
              <th scope="col">PBlk</th>
              <th scope="col">RBlk</th>
              <th scope="col">End</th>
              <th scope="col">KPow</th>
              <th scope="col">KAcc</th>
            </tr>
          </thead>
          <tbody>
            {playerList.payload.map((p) => (
              <tr key={p.id}>
                <td>
                  <Link to={`/players/${p.id}`}>{p.id}</Link>
                </td>
                <td>{p.user ? p.user.forumUsername : p.userId}</td>
                <td>{p.draftSeasonId}</td>
                <td>{p.team ? p.team.acronym : p.teamId}</td>
                <td>
                  <Link to={`/players/${p.id}`}>
                    {p.firstName} {p.lastName}
                  </Link>
                </td>
                <td>{p.position}</td>
                <td>TODO</td>
                <td>TODO</td>
                <td>TODO</td>
                <td>TODO</td>
                <td>{p.strength}</td>
                <td>{p.agility}</td>
                <td>{p.arm}</td>
                <td>{p.intelligence}</td>
                <td>{p.accuracy}</td>
                <td>{p.tackling}</td>
                <td>{p.speed}</td>
                <td>{p.hands}</td>
                <td>{p.passBlocking}</td>
                <td>{p.runBlocking}</td>
                <td>{p.endurance}</td>
                <td>{p.kickDistance}</td>
                <td>{p.kickAccuracy}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      )}
    </div>
  );
};

export default PlayersList;
