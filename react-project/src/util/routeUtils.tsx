import React, { useContext } from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { useObserver } from "mobx-react";
import { UserStoreContext } from "../contexts/UserStoreContext";

export const PrivateRoute: React.FC<RouteProps> = ({ children, ...rest }) => {
  const store = useContext(UserStoreContext);

  return useObserver(() => (
    <Route {...rest}>
      {store.user ? children : <Redirect to={{ pathname: "/" }} />}
    </Route>
  ));
};
