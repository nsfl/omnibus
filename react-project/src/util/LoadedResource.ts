import { ApiError } from "../services/api/api-call-service";

interface ResourceInit {
  status: "init";
}
interface ResourceLoading {
  status: "loading";
}
interface ResourceLoaded<T> {
  status: "loaded";
  payload: T;
}
interface ResourceError {
  status: "error";
  error: ApiError;
}
export type Resource<T> =
  | ResourceInit
  | ResourceLoading
  | ResourceLoaded<T>
  | ResourceError;
