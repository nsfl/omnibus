#!/bin/bash

LHOST=${LHOST:-"localhost"}
LPORT=${LPORT:-"5432"}
RHOST=${RHOST:-"192.168.0.3"}
RPORT=${RPORT:-"5432"}
REMOTE=${REMOTE:-"postgres@duckblade.com"}
REMOTEP=${REMOTEP:-"10987"}

echo "Starting Postgres tunnel connection to $RHOST:$RPORT via $REMOTE:$REMOTEP, exposing locally as $LHOST:$LPORT"
ssh -N -L "$LHOST:$LPORT:$RHOST:$RPORT" "$REMOTE" -p "$REMOTEP"
