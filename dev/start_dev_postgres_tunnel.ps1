$LHOST=$env:LHOST; if ($null -eq $LHOST) { $LHOST="localhost" }
$LPORT=$env:LPORT; if ($null -eq $LPORT) { $LPORT="5432" }
$RHOST=$env:RHOST; if ($null -eq $RHOST) { $RHOST="192.168.0.3" }
$RPORT=$env:RPORT; if ($null -eq $RPORT) { $RPORT="5432" }
$REMOTE=$env:REMOTE; if ($null -eq $REMOTE) { $REMOTE="postgres@duckblade.com" }
$REMOTEP=$env:REMOTEP; if ($null -eq $REMOTEP) { $REMOTEP="10987" }

Write-Output "Starting Postgres tunnel connection to ${RHOST}:${RPORT} via ${REMOTE}:${REMOTEP}, exposing locally as ${LHOST}:${LPORT}"
ssh -N -L "${LHOST}:${LPORT}:${RHOST}:${RPORT}" "$REMOTE" -p "$REMOTEP"
