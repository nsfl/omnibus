package com.simfootball.nsfl.services.auth

import com.simfootball.nsfl.model.db.User
import com.simfootball.nsfl.test_util.singleKoin
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import org.koin.test.KoinTest
import org.koin.test.inject

class UserAuthenticationServiceTest : FunSpec(), KoinTest {

    private val userAuthenticationService: UserAuthenticationService by inject()

    init {
        singleKoin { UserAuthenticationService() }

        test("should update user passwords with new values") {
            val u = User {}
            userAuthenticationService.updatePassword(u, "1234")

            u.passwordHash shouldNotBe null
            println(u.passwordHash)
        }

        test("should correctly verify password hashes") {
            val u = User {
                passwordHash = "\$2a\$12\$1Y73cVsMgHFkJn7qhwMmzO/2FGojaD41hEZIvrBSaKOQsu6O4WfhO"
            }

            userAuthenticationService.checkPassword(u, "1234") shouldBe true
            userAuthenticationService.checkPassword(u, "12345") shouldBe false
        }
    }
}
