package com.simfootball.nsfl.services.auth

import com.simfootball.nsfl.ReqContext
import com.simfootball.nsfl.model.db.User
import com.simfootball.nsfl.services.user
import com.simfootball.nsfl.test_util.singleKoin
import io.kotest.assertions.throwables.shouldNotThrowAny
import io.kotest.assertions.throwables.shouldThrowExactly
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.throwable.shouldHaveMessage
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.path
import io.ktor.util.KtorExperimentalAPI
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import org.koin.test.KoinTest
import org.koin.test.inject

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
@Suppress("unused")
class AuthorizationServiceTest : FunSpec(), KoinTest {

    private val authorizationService: AuthorizationService by inject()
    private val context: ReqContext = mockk(relaxed = true)

    private val computeSlot = slot<() -> MutableMap<String, Boolean>>()

    init {
        singleKoin { AuthorizationService() }

        beforeTest {
            every { context.call.request.path() } returns "dummyPath"
            every { context.call.attributes.getOrNull<MutableMap<String, Boolean>>(any()) } returns null
            every { context.call.attributes.computeIfAbsent<MutableMap<String, Boolean>>(any(), any()) } returns
                mutableMapOf()
        }

        test("should fail when unauthenticated") {
            every { context.user } returns null

            authorizationService.checkAuth(context) shouldBe false
            authorizationService.checkAuth(context, AuthorizationScope({ true })) shouldBe false
            shouldThrowExactly<UnauthenticatedException> {
                authorizationService.requireAuth(context)
            }
            shouldThrowExactly<UnauthenticatedException> {
                authorizationService.requireAuth(context, AuthorizationScope({ true }))
            }
        }

        test("should fail when unauthorized") {
            every { context.user } returns User {}

            authorizationService.checkAuth(context, AuthorizationScope({ false })) shouldBe false
            val ex = shouldThrowExactly<UnauthorizedException> {
                authorizationService.requireAuth(context, AuthorizationScope({ false }))
            }
            ex shouldHaveMessage "Not permitted access to dummyPath"
        }

        test("should succeed when authorized") {
            every { context.user } returns User {}

            authorizationService.checkAuth(context, AuthorizationScope({ true })) shouldBe true
            shouldNotThrowAny {
                authorizationService.requireAuth(context, AuthorizationScope({ true }))
            }
        }

        test("should succeed when authorized implicitly via ADMIN") {
            every { context.user } returns User { isAdmin = true }

            authorizationService.checkAuth(context, AuthorizationScope({ false })) shouldBe true
            shouldNotThrowAny {
                authorizationService.requireAuth(context, AuthorizationScope({ false }))
            }
        }
    }
}
