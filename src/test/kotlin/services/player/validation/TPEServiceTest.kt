package com.simfootball.nsfl.services.player.validation

import com.simfootball.nsfl.model.db.Archetype
import com.simfootball.nsfl.model.db.Player
import com.simfootball.nsfl.test_util.singleKoin
import io.kotest.core.spec.style.FunSpec
import io.kotest.data.forAll
import io.kotest.data.row
import io.kotest.matchers.ints.shouldBeExactly
import org.koin.test.KoinTest
import org.koin.test.inject

class TPEServiceTest : FunSpec(), KoinTest {

    private val playerTpeCalculatorService: PlayerTPECalculatorService by inject()

    init {
        singleKoin { PlayerTPECalculatorService() }
        tpeCostTests()
        upgradeCostTests()
        totalTPESpentTests()
    }

    private fun tpeCostTests() = test("Should correctly calculate TPE Attribute Costs") {
        forAll(
            row(1, 1),
            row(10, 10),
            row(35, 35),
            row(50, 50),
            row(51, 52),
            row(60, 70),
            row(70, 90),
            row(71, 95),
            row(80, 140),
            row(81, 150),
            row(90, 240),
            row(91, 255),
            row(100, 390)
        ) { value, expectedCost ->
            val cost = playerTpeCalculatorService.getTPECost(value)
            cost shouldBeExactly expectedCost
        }
    }

    private fun upgradeCostTests() = test("Should correctly calculate TPE Upgrade Costs") {
        forAll(
            row(1, 10, 9),
            row(10, 20, 10),
            row(5, 27, 22),
            row(30, 54, 28),
            row(40, 50, 10),
            row(40, 51, 12),
            row(50, 60, 20),
            row(55, 70, 30),
            row(70, 81, 60),
            row(80, 91, 115),
            row(90, 100, 150),
            row(40, 75, 75),
            row(53, 87, 154),
            row(72, 93, 185),
            row(85, 99, 185),
            row(40, 100, 350)
        ) { oldValue, newValue, expectedCost ->
            val cost = playerTpeCalculatorService.getUpgradeCost(oldValue, newValue)
            cost shouldBeExactly expectedCost
        }
    }

    private fun totalTPESpentTests() = test("Should correctly calculate Total TPE Spent") {
        val total = playerTpeCalculatorService.getTotalTPESpent(dummyPlayer, dummyArchetype)
        total shouldBeExactly 50
    }

    private val dummyArchetype = Archetype {
        baseStrength = 55
        baseAgility = 60
        baseArm = 1
        baseSpeed = 65
        baseHands = 25
        baseIntelligence = 45
        baseAccuracy = 1
        basePassBlocking = 1
        baseRunBlocking = 1
        baseTackling = 60
        baseKickDistance = 1
        baseKickAccuracy = 1
        baseEndurance = 60
    }

    private val dummyPlayer = Player {
        strength = 60
        agility = 60
        arm = 1
        speed = 72
        hands = 26
        intelligence = 57
        accuracy = 1
        passBlocking = 1
        runBlocking = 1
        tackling = 60
        kickDistance = 1
        kickAccuracy = 1
        endurance = 60
    }
}
