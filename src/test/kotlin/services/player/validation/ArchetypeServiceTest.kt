package com.simfootball.nsfl.services.player.validation

import com.simfootball.nsfl.model.db.Archetype
import com.simfootball.nsfl.model.db.Player
import com.simfootball.nsfl.test_util.singleKoin
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import org.koin.test.KoinTest
import org.koin.test.inject

@Suppress("unused")
class ArchetypeServiceTest : FunSpec(), KoinTest {

    // spyk uses default implementations where provided
    private val archetypeService: ArchetypeService by inject()

    init {
        singleKoin { ArchetypeService() }

        test("should return null when no errors are present") {
            val result = archetypeService.checkAllAttributes(conformingPlayer, dummyArchetype)
            result shouldBe null
        }

        test("should return errors when stats are below required") {
            val underResult = archetypeService.checkAllAttributes(underStatPlayer, dummyArchetype)
            underResult shouldContainExactly allErrors
        }

        test("should return errors when stats are above required") {
            val overResult = archetypeService.checkAllAttributes(overStatPlayer, dummyArchetype)
            overResult shouldContainExactly allErrors
        }
    }

    private val allErrors = listOf(
        "Height",
        "Weight",
        "Strength",
        "Agility",
        "Arm",
        "Speed",
        "Hands",
        "Intelligence",
        "Accuracy",
        "PassBlocking",
        "RunBlocking",
        "Tackling",
        "KickDistance",
        "KickAccuracy",
        "Endurance"
    )

    private val dummyArchetype = Archetype {
        minHeight = 3
        maxHeight = 10
        minWeight = 3
        maxWeight = 10
        baseStrength = 3
        baseAgility = 3
        baseArm = 3
        baseSpeed = 3
        baseHands = 3
        baseIntelligence = 3
        baseAccuracy = 3
        basePassBlocking = 3
        baseRunBlocking = 3
        baseTackling = 3
        baseKickDistance = 3
        baseKickAccuracy = 3
        baseEndurance = 3
        maxStrength = 10
        maxAgility = 10
        maxArm = 10
        maxSpeed = 10
        maxHands = 10
        maxIntelligence = 10
        maxAccuracy = 10
        maxPassBlocking = 10
        maxRunBlocking = 10
        maxTackling = 10
        maxKickDistance = 10
        maxKickAccuracy = 10
        maxEndurance = 10
    }

    private fun createPlayer(stat: Int) = Player {
        height = stat
        weight = stat
        strength = stat
        agility = stat
        arm = stat
        speed = stat
        hands = stat
        intelligence = stat
        accuracy = stat
        passBlocking = stat
        runBlocking = stat
        tackling = stat
        kickDistance = stat
        kickAccuracy = stat
        endurance = stat
    }

    private val conformingPlayer = createPlayer(5)

    private val underStatPlayer = createPlayer(1)

    private val overStatPlayer = createPlayer(20)
}
