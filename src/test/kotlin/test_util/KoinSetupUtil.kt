@file:Suppress("unused")
package com.simfootball.nsfl.test_util

import io.kotest.core.spec.Spec
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.core.definition.Definition
import org.koin.dsl.ModuleDeclaration
import org.koin.dsl.module

/**
 * Utility to start up koin contexts that contain only services (no properties init).
 * Useful for when the service you're testing has a few basic dependencies,
 * but doesn't need a lengthy Koin definition.
 */
fun Spec.simpleKoin(decl: ModuleDeclaration) {
    beforeSpec {
        startKoin {
            modules(module(moduleDeclaration = decl))
        }
    }

    afterSpec { stopKoin() }
}

/**
 * Utility to create single-dependency koin scopes.
 * Useful for when the service you're testing has no dependencies.
 */
inline fun <reified T> Spec.singleKoin(noinline definition: Definition<T>) {
    simpleKoin { single(definition = definition) }
}
