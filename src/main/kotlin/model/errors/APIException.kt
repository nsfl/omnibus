package com.simfootball.nsfl.model.errors

/**
 * Represents a known error within the API.
 * Exceptions thrown of this type will be handled
 * by the APIExceptionResponder, or a more specific variant.
 * Messages will be returned to the caller in JSON.
 */
open class APIException(vararg val messages: String) : Exception(messages.joinToString(", "))
