package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.enum
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.typeRef

enum class UserRole {
    GM,
    BANKER,
    SIMMER
}

object UserRoleAssignments : Table<UserRoleAssignment>("users") {

    val user by int("user").bindTo { it.userId }.references(Users) { it.user }
    val role by enum("role", typeRef<UserRole>()).bindTo { it.role }
}

interface UserRoleAssignment : Entity<UserRoleAssignment> {
    companion object : Entity.Factory<UserRoleAssignment>()

    var userId: Int
    var user: User
    var role: UserRole
}
