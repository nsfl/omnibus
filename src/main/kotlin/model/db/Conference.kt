package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.entity.filter
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar

object Conferences : IntIdTable<Conference>("conferences") {

    val name by varchar("name").bindTo { it.name }
    val acronym by varchar("acronym").bindTo { it.acronym }
    val league by int("league").bindTo { it.leagueId }.references(Leagues) { it.league }
}

interface Conference : IntEntity<Conference> {
    companion object : Entity.Factory<Conference>()

    var name: String
    var acronym: String
    var leagueId: Int
    val league: League
}

fun Conference.teams(db: Database): List<Team> {
    return db.sequenceOf(Teams).filter { it.conference eq this.id!! }.toList()
}
