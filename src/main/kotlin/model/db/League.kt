package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.dsl.from
import me.liuwj.ktorm.dsl.leftJoin
import me.liuwj.ktorm.dsl.select
import me.liuwj.ktorm.dsl.where
import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.varchar

object Leagues : IntIdTable<League>("leagues") {

    val name by varchar("name").bindTo { it.name }
    val acronym by varchar("acronym").bindTo { it.acronym }
}

interface League : IntEntity<League> {
    companion object : Entity.Factory<League>()

    var name: String
    var acronym: String
}

fun League.teams(db: Database): List<Team> {
    return db
        .from(Teams)
        .leftJoin(Conferences, Teams.conference eq Conferences.id)
        .select()
        .where { Conferences.league eq id!! }
        .map { row -> Teams.createEntity(row) }
}
