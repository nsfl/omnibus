package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.enum
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.text
import me.liuwj.ktorm.schema.typeRef

enum class TPEClaimStatus {
    PENDING,
    ACCEPTED,
    REJECTED
}

object TPEClaims : IntIdTable<TPEClaim>("tpe_claims") {

    val claimant by int("claimant").bindTo { it.claimantId }.references(Players) { it.claimant }
    val tpeTask by int("tpe_task").bindTo { it.tpeTaskId }.references(TPETasks) { it.tpeTask }
    val link by text("link").bindTo { it.link }
    val status = enum("status", typeRef<TPEClaimStatus>())
    val responder by int("responder").bindTo { it.responderId }.references(Users) { it.responder }
}

interface TPEClaim : IntEntity<TPEClaim> {
    companion object : Entity.Factory<TPEClaim>()

    var claimantId: Int
    val claimant: Player
    var tpeTaskId: Int?
    val tpeTask: TPETask?
    var link: String
    val status: TPEClaimStatus
    var responderId: Int?
    val responder: User?
}
