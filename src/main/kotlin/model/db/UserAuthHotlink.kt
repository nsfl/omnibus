package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.text

object UserAuthHotlinks : Table<UserAuthHotlink>("user_auth_hotlinks") {

    val targetUser by int("target_user").bindTo { it.targetUserId }.references(Users) { it.targetUser }.primaryKey()
    val payload by text("payload").bindTo { it.payload }
}

interface UserAuthHotlink : IntEntity<UserAuthHotlink> {
    companion object : Entity.Factory<UserAuthHotlink>()

    var targetUserId: Int
    val targetUser: User
    var payload: String
}
