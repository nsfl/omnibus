package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.entity.filter
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import me.liuwj.ktorm.schema.boolean
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.text
import me.liuwj.ktorm.schema.varchar

object Users : IntIdTable<User>("users") {

    val username by varchar("username").bindTo { it.username }
    val passwordHash by text("password_hash").bindTo { it.passwordHash }
    val forumUsername by varchar("forum_username").bindTo { it.forumUsername }
    val isAdmin by boolean("is_admin").bindTo { it.isAdmin }
    val bankAccount by int("bank_account").bindTo { it.bankAccountId }.references(BankAccounts) { it.bankAccount }
}

interface User : IntEntity<User> {
    companion object : Entity.Factory<User>()

    var username: String
    var passwordHash: String
    var forumUsername: String
    var isAdmin: Boolean
    var bankAccountId: Int
    val bankAccount: BankAccount
}

fun User.players(db: Database): List<Player> {
    return db.sequenceOf(Players).filter { it.user eq this.id!! }.toList()
}
