package com.simfootball.nsfl.model.db

import java.time.Instant
import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.text
import me.liuwj.ktorm.schema.timestamp

object PlayerHistories : IntIdTable<PlayerHistory>("player_histories") {

    val player by int("player").bindTo { it.playerId }.references(Players) { it.player }
    val revisionTimestamp by timestamp("revision_timestamp").bindTo { it.revisionTimestamp }
    val changes by text("changes").bindTo { it.changes }
    val author by int("author").bindTo { it.authorId }.references(Users) { it.author }
    val reason by text("reason").bindTo { it.reason }
}

interface PlayerHistory : IntEntity<PlayerHistory> {
    companion object : Entity.Factory<PlayerHistory>()

    var playerId: Int
    val player: Player
    var revisionTimestamp: Instant
    var changes: String
    var authorId: Int?
    val author: User?
    var reason: String?
}
