package com.simfootball.nsfl.model.db

import java.time.LocalDate
import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.date

object Seasons : IntIdTable<Season>("seasons") {

    val startDate by date("start_date").bindTo { it.startDate }
    val endDate by date("end_date").bindTo { it.endDate }
    val simStartDate by date("sim_start_date").bindTo { it.simStartDate }
    val simEndDate by date("sim_end_date").bindTo { it.simEndDate }
    val offSeasonStartDate by date("off_season_start_date").bindTo { it.offSeasonStartDate }
}

interface Season : IntEntity<Season> {
    companion object : Entity.Factory<Season>()

    var startDate: LocalDate
    var endDate: LocalDate
    var simStartDate: LocalDate
    var simEndDate: LocalDate
    var offSeasonStartDate: LocalDate
}
