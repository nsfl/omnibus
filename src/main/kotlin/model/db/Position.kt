package com.simfootball.nsfl.model.db

enum class Position {
    QB,
    RB,
    WR,
    TE,
    OL,
    DE,
    DT,
    LB,
    CB,
    S,
    KP
}
