package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.boolean
import me.liuwj.ktorm.schema.enum
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.typeRef
import me.liuwj.ktorm.schema.varchar

object Players : IntIdTable<Player>("players") {

    val user by int("user").bindTo { it.userId }.references(Users) { it.user }
    val draftSeason by int("draft_season").bindTo { it.draftSeasonId }.references(Seasons) { it.draftSeason }
    val retirementSeason by int("retirement_season").bindTo { it.retirementSeasonId }
        .references(Seasons) { it.retirementSeason }
    val active by boolean("active").bindTo { it.active }
    val tpeSpent by int("tpe_spent").bindTo { it.tpeSpent }
    val tpeBanked by int("tpe_banked").bindTo { it.tpeBanked }
    val position = enum("position", typeRef<Position>()).bindTo { it.position }
    val archetype by int("archetype").bindTo { it.archetypeId }.references(Archetypes) { it.archetype }
    val firstName by varchar("first_name").bindTo { it.firstName }
    val lastName by varchar("last_name").bindTo { it.lastName }
    val team by int("team").bindTo { it.teamId }.references(Teams) { it.team }
    val college by varchar("college").bindTo { it.college }
    val jerseyNumber by int("jersey_number").bindTo { it.jerseyNumber }
    val age by int("age").bindTo { it.age }
    val experience by int("experience").bindTo { it.experience }
    val height by int("height").bindTo { it.height }
    val weight by int("weight").bindTo { it.weight }
    val strength by int("strength").bindTo { it.strength }
    val agility by int("agility").bindTo { it.agility }
    val arm by int("arm").bindTo { it.arm }
    val speed by int("speed").bindTo { it.speed }
    val hands by int("hands").bindTo { it.hands }
    val intelligence by int("intelligence").bindTo { it.intelligence }
    val accuracy by int("accuracy").bindTo { it.accuracy }
    val passBlocking by int("pass_blocking").bindTo { it.passBlocking }
    val runBlocking by int("run_blocking").bindTo { it.runBlocking }
    val tackling by int("tackling").bindTo { it.tackling }
    val kickDistance by int("kick_distance").bindTo { it.kickDistance }
    val kickAccuracy by int("kick_accuracy").bindTo { it.kickAccuracy }
    val endurance by int("endurance").bindTo { it.endurance }
}

interface Player : IntEntity<Player> {
    companion object : Entity.Factory<Player>()

    var userId: Int
    val user: User
    var draftSeasonId: Int
    val draftSeason: Season
    var retirementSeasonId: Int?
    val retirementSeason: Season?
    var active: Boolean
    var tpeSpent: Int
    var tpeBanked: Int
    var position: Position
    var archetypeId: Int
    val archetype: Archetype
    var firstName: String
    var lastName: String
    var teamId: Int?
    val team: Team?
    var college: String
    var jerseyNumber: Int
    var age: Int
    var experience: Int
    var height: Int
    var weight: Int
    var strength: Int
    var agility: Int
    var arm: Int
    var speed: Int
    var hands: Int
    var intelligence: Int
    var accuracy: Int
    var passBlocking: Int
    var runBlocking: Int
    var tackling: Int
    var kickDistance: Int
    var kickAccuracy: Int
    var endurance: Int
}
