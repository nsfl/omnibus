package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.enum
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.text
import me.liuwj.ktorm.schema.typeRef
import me.liuwj.ktorm.schema.varchar

object Archetypes : IntIdTable<Archetype>("archetypes") {

    val position by enum("position", typeRef<Position>()).bindTo { it.position }
    val name by varchar("name").bindTo { it.name }
    val examplePlayers by text("example_players").bindTo { it.examplePlayers }
    val minHeight by int("min_height").bindTo { it.minHeight }
    val maxHeight by int("max_height").bindTo { it.maxHeight }
    val minWeight by int("min_weight").bindTo { it.minWeight }
    val maxWeight by int("max_weight").bindTo { it.maxWeight }
    val expBase by int("exp_base").bindTo { it.expBase }
    val expPerYear by int("exp_per_year").bindTo { it.expPerYear }
    val expPerTpeMilestone by int("exp_per_tpe_milestone").bindTo { it.expPerTpeMilestone }
    val expTpeMilestoneAmount by int("exp_tpe_milestone_amount").bindTo { it.expTpeMilestoneAmount }
    val baseStrength by int("base_strength").bindTo { it.baseStrength }
    val baseAgility by int("base_agility").bindTo { it.baseAgility }
    val baseArm by int("base_arm").bindTo { it.baseArm }
    val baseSpeed by int("base_speed").bindTo { it.baseSpeed }
    val baseHands by int("base_hands").bindTo { it.baseHands }
    val baseIntelligence by int("base_intelligence").bindTo { it.baseIntelligence }
    val baseAccuracy by int("base_accuracy").bindTo { it.baseAccuracy }
    val basePassBlocking by int("base_pass_blocking").bindTo { it.basePassBlocking }
    val baseRunBlocking by int("base_run_blocking").bindTo { it.baseRunBlocking }
    val baseTackling by int("base_tackling").bindTo { it.baseTackling }
    val baseKickDistance by int("base_kick_distance").bindTo { it.baseKickDistance }
    val baseKickAccuracy by int("base_kick_accuracy").bindTo { it.baseKickAccuracy }
    val baseEndurance by int("base_endurance").bindTo { it.baseEndurance }
    val maxStrength by int("max_strength").bindTo { it.maxStrength }
    val maxAgility by int("max_agility").bindTo { it.maxAgility }
    val maxArm by int("max_arm").bindTo { it.maxArm }
    val maxSpeed by int("max_speed").bindTo { it.maxSpeed }
    val maxHands by int("max_hands").bindTo { it.maxHands }
    val maxIntelligence by int("max_intelligence").bindTo { it.maxIntelligence }
    val maxAccuracy by int("max_accuracy").bindTo { it.maxAccuracy }
    val maxPassBlocking by int("max_pass_blocking").bindTo { it.maxPassBlocking }
    val maxRunBlocking by int("max_run_blocking").bindTo { it.maxRunBlocking }
    val maxTackling by int("max_tackling").bindTo { it.maxTackling }
    val maxKickDistance by int("max_kick_distance").bindTo { it.maxKickDistance }
    val maxKickAccuracy by int("max_kick_accuracy").bindTo { it.maxKickAccuracy }
    val maxEndurance by int("max_endurance").bindTo { it.maxEndurance }
}

interface Archetype : IntEntity<Archetype> {
    companion object : Entity.Factory<Archetype>()

    var position: Position
    var name: String
    var examplePlayers: String
    var minHeight: Int
    var maxHeight: Int
    var minWeight: Int
    var maxWeight: Int
    var expBase: Int
    var expPerYear: Int
    var expPerTpeMilestone: Int
    var expTpeMilestoneAmount: Int
    var baseStrength: Int
    var baseAgility: Int
    var baseArm: Int
    var baseSpeed: Int
    var baseHands: Int
    var baseIntelligence: Int
    var baseAccuracy: Int
    var basePassBlocking: Int
    var baseRunBlocking: Int
    var baseTackling: Int
    var baseKickDistance: Int
    var baseKickAccuracy: Int
    var baseEndurance: Int
    var maxStrength: Int
    var maxAgility: Int
    var maxArm: Int
    var maxSpeed: Int
    var maxHands: Int
    var maxIntelligence: Int
    var maxAccuracy: Int
    var maxPassBlocking: Int
    var maxRunBlocking: Int
    var maxTackling: Int
    var maxKickDistance: Int
    var maxKickAccuracy: Int
    var maxEndurance: Int
}
