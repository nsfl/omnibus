package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.enum
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.text
import me.liuwj.ktorm.schema.typeRef

enum class BankAccountTransactionStatus {
    PENDING,
    ACCEPTED,
    REJECTED
}

object BankAccountTransactions : IntIdTable<BankAccountTransaction>("bank_account_transactions") {

    val recipient by int("recipient").bindTo { it.recipientId }.references(BankAccounts) { it.recipient }
    val amount by int("amount").bindTo { it.amount }
    val sender by int("sender").bindTo { it.senderId }.references(BankAccounts) { it.sender }
    val notes by text("notes").bindTo { it.notes }
    val status = enum("status", typeRef<BankAccountTransactionStatus>())
    val responder by int("responder").bindTo { it.responderId }.references(Users) { it.responder }
    val season by int("season").bindTo { it.seasonId }.references(Seasons) { it.season }
}

interface BankAccountTransaction : IntEntity<BankAccountTransaction> {
    companion object : Entity.Factory<BankAccountTransaction>()

    var recipientId: Int
    val recipient: BankAccount
    var amount: Int
    var senderId: Int?
    val sender: BankAccount?
    var notes: String?
    var status: BankAccountTransactionStatus
    var responderId: Int?
    val responder: User?
    var seasonId: Int
    val season: Season
}
