package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.entity.find
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.schema.Column
import me.liuwj.ktorm.schema.Table
import me.liuwj.ktorm.schema.int

abstract class IdTable<T : Any, Q : Entity<Q>>(tableName: String) : Table<Q>(tableName) {
    abstract val id: Column<T>

    abstract fun findById(id: T, database: Database): Q?
}

abstract class IntIdTable<Q : IntEntity<Q>>(tableName: String, columnName: String = "id") : IdTable<Int, Q>(tableName) {
    override val id by int(columnName).bindTo { it.id }.primaryKey()

    override fun findById(id: Int, database: Database): Q? {
        return database.sequenceOf(this)
            .find { it.id eq id }
    }
}

interface IdEntity<T : Any, Q : IdEntity<T, Q>> : Entity<Q> {
    // for jvm primitives, this will need to be overridden
    // by the subclass in order to properly cast for jackson
    var id: T?
}

interface IntEntity<Q : IntEntity<Q>> : IdEntity<Int, Q> {
    override var id: Int?
}
