package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.enum
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.text
import me.liuwj.ktorm.schema.typeRef

enum class TPETaskStatus {
    PENDING,
    OPEN,
    CLOSED,
    REJECTED
}

object TPETasks : IntIdTable<TPETask>("tpe_tasks") {

    val creator by int("creator").bindTo { it.creatorId }.references(Users) { it.creator }
    val name by text("name").bindTo { it.name }
    val description by text("description").bindTo { it.description }
    val link by text("link").bindTo { it.link }
    val status = enum("status", typeRef<TPETaskStatus>())
    val claimLink by text("claim_link").bindTo { it.claimLink }
    val season by int("season").bindTo { it.seasonId }.references(Seasons) { it.season }
}

interface TPETask : IntEntity<TPETask> {
    companion object : Entity.Factory<TPETask>()

    var creatorId: Int?
    val creator: User?
    var name: String
    var description: String
    var link: String?
    var status: TPETaskStatus
    var claimLink: String
    var seasonId: Int
    val season: Season
}
