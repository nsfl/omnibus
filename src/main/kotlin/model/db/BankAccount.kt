package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.schema.int

object BankAccounts : IntIdTable<BankAccount>("bank_accounts") {

    val balance by int("balance").bindTo { it.balance }
}

interface BankAccount : IntEntity<BankAccount> {
    companion object : Entity.Factory<BankAccount>()

    var balance: Int
}
