package com.simfootball.nsfl.model.db

import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.Entity
import me.liuwj.ktorm.entity.filter
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import me.liuwj.ktorm.schema.int
import me.liuwj.ktorm.schema.varchar

object Teams : IntIdTable<Team>("teams") {

    val city by varchar("city").bindTo { it.city }
    val mascot by varchar("mascot").bindTo { it.mascot }
    val acronym by varchar("acronym").bindTo { it.acronym }
    val conference by int("conference").bindTo { it.conferenceId }.references(Conferences) { it.conference }
    val owner by int("owner").bindTo { it.ownerId }.references(Users) { it.owner }
    val bankAccount by int("bank_account").bindTo { it.bankAccountId }.references(BankAccounts) { it.bankAccount }
    val startSeason by int("start_season").bindTo { it.startSeasonId }.references(Seasons) { it.startSeason }
    val endSeason by int("end_season").bindTo { it.endSeasonId }.references(Seasons) { it.endSeason }
}

interface Team : IntEntity<Team> {
    companion object : Entity.Factory<Team>()

    var city: String
    var mascot: String
    var acronym: String
    var conferenceId: Int
    val conference: Conference
    var ownerId: Int?
    val owner: User?
    var bankAccountId: Int
    val bankAccount: BankAccount
    var startSeasonId: Int
    val startSeason: Season
    var endSeasonId: Int?
    val endSeason: Season?
}

fun Team.players(db: Database): List<Player> {
    return db.sequenceOf(Players).filter { it.team eq this.id!! }.toList()
}
