package com.simfootball.nsfl.model.rest.requests.player

import com.simfootball.nsfl.model.db.Player
import com.simfootball.nsfl.model.db.Position
import kotlin.reflect.KMutableProperty1
import kotlin.reflect.KProperty1

data class EditPlayerRequest(
    val draftSeasonId: Int?,
    val active: Boolean?,
    val firstName: String?,
    val lastName: String?,
    val college: String?,
    val jerseyNumber: Int?,
    val age: Int?,
    val tpeSpent: Int?,
    val tpeBanked: Int?,
    val position: Position?,
    val archetypeId: Int?,
    val experience: Int?,
    val height: Int?,
    val weight: Int?,
    val strength: Int?,
    val agility: Int?,
    val arm: Int?,
    val speed: Int?,
    val hands: Int?,
    val intelligence: Int?,
    val accuracy: Int?,
    val passBlocking: Int?,
    val runBlocking: Int?,
    val tackling: Int?,
    val kickDistance: Int?,
    val kickAccuracy: Int?,
    val endurance: Int?,
    val reason: String?
) {

    private inline fun <reified T : Any> checkUpdateSet(
        map: MutableMap<KProperty1<Player, *>, Any>,
        player: Player,
        playerProp: KMutableProperty1<Player, T>,
        newValue: T?
    ) {
        if (newValue != null && playerProp.get(player) != newValue) {
            playerProp.set(player, newValue)
            map[playerProp] = newValue
        }
    }

    fun applyToPlayer(player: Player): Map<KProperty1<Player, *>, Any> {
        val modified = mutableMapOf<KProperty1<Player, *>, Any>()

        checkUpdateSet(modified, player, Player::draftSeasonId, this.draftSeasonId)
        checkUpdateSet(modified, player, Player::active, this.active)
        checkUpdateSet(modified, player, Player::tpeSpent, this.tpeSpent)
        checkUpdateSet(modified, player, Player::tpeBanked, this.tpeBanked)
        checkUpdateSet(modified, player, Player::position, this.position)
        checkUpdateSet(modified, player, Player::archetypeId, this.archetypeId)
        checkUpdateSet(modified, player, Player::firstName, this.firstName)
        checkUpdateSet(modified, player, Player::lastName, this.lastName)
        checkUpdateSet(modified, player, Player::college, this.college)
        checkUpdateSet(modified, player, Player::jerseyNumber, this.jerseyNumber)
        checkUpdateSet(modified, player, Player::age, this.age)
        checkUpdateSet(modified, player, Player::experience, this.experience)
        checkUpdateSet(modified, player, Player::height, this.height)
        checkUpdateSet(modified, player, Player::weight, this.weight)
        checkUpdateSet(modified, player, Player::strength, this.strength)
        checkUpdateSet(modified, player, Player::agility, this.agility)
        checkUpdateSet(modified, player, Player::arm, this.arm)
        checkUpdateSet(modified, player, Player::speed, this.speed)
        checkUpdateSet(modified, player, Player::hands, this.hands)
        checkUpdateSet(modified, player, Player::intelligence, this.intelligence)
        checkUpdateSet(modified, player, Player::accuracy, this.accuracy)
        checkUpdateSet(modified, player, Player::passBlocking, this.passBlocking)
        checkUpdateSet(modified, player, Player::runBlocking, this.runBlocking)
        checkUpdateSet(modified, player, Player::tackling, this.tackling)
        checkUpdateSet(modified, player, Player::kickDistance, this.kickDistance)
        checkUpdateSet(modified, player, Player::kickAccuracy, this.kickAccuracy)
        checkUpdateSet(modified, player, Player::endurance, this.endurance)

        return modified
    }
}
