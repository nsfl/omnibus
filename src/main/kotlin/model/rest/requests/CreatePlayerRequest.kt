package com.simfootball.nsfl.model.rest.requests

import com.simfootball.nsfl.model.db.Player
import com.simfootball.nsfl.model.db.Position
import com.simfootball.nsfl.model.db.User

data class CreatePlayerRequest(
    val draftSeasonId: Int,
    val tpeSpent: Int,
    val tpeBanked: Int,
    val position: Position,
    val archetypeId: Int,
    val firstName: String,
    val lastName: String,
    val college: String,
    val jerseyNumber: Int,
    val age: Int,
    val experience: Int,
    val height: Int,
    val weight: Int,
    val strength: Int,
    val agility: Int,
    val arm: Int,
    val speed: Int,
    val hands: Int,
    val intelligence: Int,
    val accuracy: Int,
    val passBlocking: Int,
    val runBlocking: Int,
    val tackling: Int,
    val kickDistance: Int,
    val kickAccuracy: Int,
    val endurance: Int
) {

    fun toPlayer(owner: User): Player {
        return Player {
            this.userId = owner.id!!
            this.active = true
            this.draftSeasonId = this@CreatePlayerRequest.draftSeasonId
            this.tpeSpent = this@CreatePlayerRequest.tpeSpent
            this.tpeBanked = this@CreatePlayerRequest.tpeBanked
            this.position = this@CreatePlayerRequest.position
            this.archetypeId = this@CreatePlayerRequest.archetypeId
            this.firstName = this@CreatePlayerRequest.firstName
            this.lastName = this@CreatePlayerRequest.lastName
            this.college = this@CreatePlayerRequest.college
            this.jerseyNumber = this@CreatePlayerRequest.jerseyNumber
            this.age = this@CreatePlayerRequest.age
            this.experience = this@CreatePlayerRequest.experience
            this.height = this@CreatePlayerRequest.height
            this.weight = this@CreatePlayerRequest.weight
            this.strength = this@CreatePlayerRequest.strength
            this.agility = this@CreatePlayerRequest.agility
            this.arm = this@CreatePlayerRequest.arm
            this.speed = this@CreatePlayerRequest.speed
            this.hands = this@CreatePlayerRequest.hands
            this.intelligence = this@CreatePlayerRequest.intelligence
            this.accuracy = this@CreatePlayerRequest.accuracy
            this.passBlocking = this@CreatePlayerRequest.passBlocking
            this.runBlocking = this@CreatePlayerRequest.runBlocking
            this.tackling = this@CreatePlayerRequest.tackling
            this.kickDistance = this@CreatePlayerRequest.kickDistance
            this.kickAccuracy = this@CreatePlayerRequest.kickAccuracy
            this.endurance = this@CreatePlayerRequest.endurance
        }
    }
}
