package com.simfootball.nsfl.model.rest.requests

import com.simfootball.nsfl.model.db.Team

data class CreateTeamRequest(
    var city: String,
    var mascot: String,
    var acronym: String,
    var conferenceId: Int,
    var ownerId: Int?,
    var startSeasonId: Int
) {

    fun toTeam(bankAccountId: Int): Team {
        return Team {
            this.city = this@CreateTeamRequest.city
            this.mascot = this@CreateTeamRequest.mascot
            this.acronym = this@CreateTeamRequest.acronym
            this.conferenceId = this@CreateTeamRequest.conferenceId
            this.ownerId = this@CreateTeamRequest.ownerId
            this.bankAccountId = bankAccountId
            this.startSeasonId = this@CreateTeamRequest.startSeasonId
        }
    }
}
