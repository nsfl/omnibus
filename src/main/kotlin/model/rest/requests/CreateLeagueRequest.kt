package com.simfootball.nsfl.model.rest.requests

import com.simfootball.nsfl.model.db.League

data class CreateLeagueRequest(
    var name: String,
    var acronym: String
) {

    fun toLeague(): League {
        return League {
            this.name = this@CreateLeagueRequest.name
            this.acronym = this@CreateLeagueRequest.acronym
        }
    }
}
