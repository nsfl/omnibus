package com.simfootball.nsfl.model.rest.requests

import com.simfootball.nsfl.model.db.User

data class CreateUserRequest(
    val username: String,
    val passwordHash: String,
    val forumUsername: String,
    val isAdmin: Boolean
) {

    fun toUser(bankAccountId: Int): User {
        return User {
            this.username = this@CreateUserRequest.username
            this.passwordHash = this@CreateUserRequest.passwordHash
            this.forumUsername = this@CreateUserRequest.forumUsername
            this.isAdmin = this@CreateUserRequest.isAdmin
            this.bankAccountId = bankAccountId
        }
    }
}
