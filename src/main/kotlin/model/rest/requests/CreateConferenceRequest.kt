package com.simfootball.nsfl.model.rest.requests

import com.simfootball.nsfl.model.db.Conference

data class CreateConferenceRequest(
    var name: String,
    var acronym: String,
    var leagueId: Int
) {

    fun toConference(): Conference {
        return Conference {
            this.name = this@CreateConferenceRequest.name
            this.acronym = this@CreateConferenceRequest.acronym
            this.leagueId = this@CreateConferenceRequest.leagueId
        }
    }
}
