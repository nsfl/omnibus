package com.simfootball.nsfl.views

import kotlinx.html.HTML
import kotlinx.html.body
import kotlinx.html.div
import kotlinx.html.head
import kotlinx.html.id
import kotlinx.html.link
import kotlinx.html.meta
import kotlinx.html.noScript
import kotlinx.html.script
import kotlinx.html.title

fun HTML.omnibusReactPage(entrypoint: String = "main") {

    // figure out if we're unpacked (dev)
    // or running out of a jar
    val inStream = Thread.currentThread().contextClassLoader.getResourceAsStream("./static/scripts/bundles/")
        ?: javaClass.getResourceAsStream("./static/scripts/bundles/")

    // find all applicable js files for this entrypoint
    val scriptList = inStream.run {
        val ret = bufferedReader()
            .lineSequence()
            .filter { it.contains(entrypoint) }
            .sortedByDescending { s -> s.count { it == '~' } }

        close()
        ret
    }

    head {
        meta(charset = "utf-8")
        meta(name = "viewport", content = "minimum-scale=1, initial-scale=1, width=device-width")
        title("Turnip Tracker")
        link(rel = "stylesheet", href = "https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap")
        link(rel = "stylesheet", href = "https://fonts.googleapis.com/icon?family=Material+Icons")
    }
    body {
        div { id = "omnibus-root" }
        noScript { +"You need to enable JavaScript to run this app." }

        for (script in scriptList) {
            script(src = "/static/scripts/bundles/$script") {}
        }
    }
}
