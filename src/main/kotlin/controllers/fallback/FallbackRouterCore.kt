package com.simfootball.nsfl.controllers.fallback

import com.simfootball.nsfl.controllers.fallback.responders.APIExceptionResponder
import com.simfootball.nsfl.controllers.fallback.responders.BadRequestException
import com.simfootball.nsfl.controllers.fallback.responders.BadRequestExceptionResponder
import com.simfootball.nsfl.controllers.fallback.responders.UnauthenticatedExceptionResponder
import com.simfootball.nsfl.controllers.fallback.responders.UnauthorizedExceptionResponder
import com.simfootball.nsfl.controllers.fallback.responders.UnknownExceptionResponder
import com.simfootball.nsfl.controllers.fallback.responders.UnsupportedOperationResponder
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.services.auth.UnauthenticatedException
import com.simfootball.nsfl.services.auth.UnauthorizedException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import me.gitlab.llamos.ktorsentry.sentry

@KtorExperimentalAPI
object FallbackRouterCore : Loggable {

    suspend fun handleEmptyResponse(context: PipelineContext<Unit, ApplicationCall>) =
        handleException(context, UnsupportedOperationException())

    suspend fun handleException(context: PipelineContext<Unit, ApplicationCall>, e: Exception) = context.run {
        logger.warn("Caught exception in fallback handler: ${e.javaClass.simpleName}")
        sentry.sendMessage("Caught exception in fallback handler: ${e.javaClass.simpleName}")

        when (e) {
            is UnsupportedOperationException -> UnsupportedOperationResponder.invoke(this, e)
            is APIException -> APIExceptionResponder.invoke(this, e)
            is UnauthenticatedException -> UnauthenticatedExceptionResponder.invoke(this, e)
            is UnauthorizedException -> UnauthorizedExceptionResponder.invoke(this, e)
            is BadRequestException -> BadRequestExceptionResponder.invoke(this, e)
            else -> {
                logger.error("Uncaught error in fallback handler", e)
                sentry.sendException(e)
                UnknownExceptionResponder.invoke(this, e)
            }
        }
    }
}
