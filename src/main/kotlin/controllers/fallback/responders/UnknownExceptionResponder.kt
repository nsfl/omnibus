package com.simfootball.nsfl.controllers.fallback.responders

import com.simfootball.nsfl.controllers.fallback.ExceptionResponder
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

object UnknownExceptionResponder : ExceptionResponder<Exception>() {

    private val responseBody = APIExceptionResponder.APIExceptionBody(messages = arrayOf("Unknown Error"))

    override suspend fun PipelineContext<Unit, ApplicationCall>.respond(cause: Exception) {
        call.respond(HttpStatusCode.InternalServerError, responseBody)
    }
}
