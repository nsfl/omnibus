package com.simfootball.nsfl.controllers.fallback.responders

import com.simfootball.nsfl.controllers.fallback.ExceptionResponder
import com.simfootball.nsfl.model.errors.APIException
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

class BadRequestException(vararg messages: String = arrayOf("Bad request")) : APIException(*messages)

object BadRequestExceptionResponder : ExceptionResponder<BadRequestException>() {
    override suspend fun PipelineContext<Unit, ApplicationCall>.respond(cause: BadRequestException) {
        call.respond(HttpStatusCode.BadRequest, APIExceptionResponder.APIExceptionBody(messages = cause.messages))
    }
}
