package com.simfootball.nsfl.controllers.fallback.responders

import com.simfootball.nsfl.controllers.fallback.ExceptionResponder
import com.simfootball.nsfl.services.auth.UnauthorizedException
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

object UnauthorizedExceptionResponder : ExceptionResponder<UnauthorizedException>() {
    override suspend fun PipelineContext<Unit, ApplicationCall>.respond(cause: UnauthorizedException) {
        val errMessage = cause.message ?: "Not permitted"
        call.respond(
            HttpStatusCode.Unauthorized,
            APIExceptionResponder.APIExceptionBody(messages = arrayOf(errMessage))
        )
    }
}
