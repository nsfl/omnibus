package com.simfootball.nsfl.controllers.fallback.responders

import com.simfootball.nsfl.controllers.fallback.ExceptionResponder
import com.simfootball.nsfl.model.errors.APIException
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

object APIExceptionResponder : ExceptionResponder<APIException>() {

    @Suppress("ArrayInDataClass") // we don't do any comparisons, so we don't need a strong equals() on this data class
    data class APIExceptionBody(val error: Boolean = true, val messages: Array<out String>)

    override suspend fun PipelineContext<Unit, ApplicationCall>.respond(cause: APIException) {
        call.respond(HttpStatusCode.InternalServerError, APIExceptionBody(messages = cause.messages))
    }
}
