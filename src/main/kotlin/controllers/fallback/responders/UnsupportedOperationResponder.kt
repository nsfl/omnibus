package com.simfootball.nsfl.controllers.fallback.responders

import com.simfootball.nsfl.controllers.fallback.ExceptionResponder
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

object UnsupportedOperationResponder : ExceptionResponder<UnsupportedOperationException>() {

    override suspend fun PipelineContext<Unit, ApplicationCall>.respond(cause: UnsupportedOperationException) {
        call.respond(HttpStatusCode.NotFound)
    }
}
