package com.simfootball.nsfl.controllers.fallback.responders

import com.simfootball.nsfl.controllers.fallback.ExceptionResponder
import com.simfootball.nsfl.services.auth.UnauthenticatedException
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond
import io.ktor.util.pipeline.PipelineContext

object UnauthenticatedExceptionResponder : ExceptionResponder<UnauthenticatedException>() {
    override suspend fun PipelineContext<Unit, ApplicationCall>.respond(cause: UnauthenticatedException) {
        call.respond(
            HttpStatusCode.Unauthorized,
            APIExceptionResponder.APIExceptionBody(messages = arrayOf("Not signed in"))
        )
    }
}
