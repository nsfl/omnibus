package com.simfootball.nsfl.controllers.fallback

import io.ktor.application.ApplicationCall
import io.ktor.util.pipeline.PipelineContext

abstract class ExceptionResponder<EType : Exception> {

    suspend fun invoke(context: PipelineContext<Unit, ApplicationCall>, cause: EType) = context.run { respond(cause) }

    abstract suspend fun PipelineContext<Unit, ApplicationCall>.respond(cause: EType)
}
