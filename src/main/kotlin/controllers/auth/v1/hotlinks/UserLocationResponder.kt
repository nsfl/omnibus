package com.simfootball.nsfl.controllers.auth.v1.hotlinks

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.UserAuthHotlink
import com.simfootball.nsfl.model.db.UserAuthHotlinks
import com.simfootball.nsfl.model.db.Users
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import java.math.BigInteger
import java.security.SecureRandom
import kotlin.random.asKotlinRandom
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.delete
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class UserLocationResponder : Responder<HotlinksLocation.UserLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<HotlinksLocation.UserLocation> = HotlinksLocation.UserLocation::class

    private val random = SecureRandom.getInstanceStrong().asKotlinRandom()
    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: HotlinksLocation.UserLocation) {
        val targetUser = Users.findById(loc.user, database)
            ?: throw IllegalArgumentException("Invalid user")

        val newHotlink = UserAuthHotlink {
            targetUserId = targetUser.id!!
            payload = BigInteger(random.nextBytes(32)).toString(36)
        }

        database.delete(UserAuthHotlinks) { it.targetUser eq targetUser.id!! }
        database.sequenceOf(UserAuthHotlinks).add(newHotlink)

        call.respond(newHotlink)
    }
}
