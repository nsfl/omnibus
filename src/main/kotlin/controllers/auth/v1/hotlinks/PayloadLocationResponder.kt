package com.simfootball.nsfl.controllers.auth.v1.hotlinks

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.UserAuthHotlinks
import com.simfootball.nsfl.services.user
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respondRedirect
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.find
import me.liuwj.ktorm.entity.sequenceOf
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
class PayloadLocationResponder : Responder<HotlinksLocation.PayloadLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<HotlinksLocation.PayloadLocation> = HotlinksLocation.PayloadLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: HotlinksLocation.PayloadLocation) {
        val payload = loc.payload

        val hotlinkInstance = database.sequenceOf(UserAuthHotlinks)
            .find { it.payload eq payload }
            ?: throw IllegalArgumentException("Bad hotlink")

        user = hotlinkInstance.targetUser
        call.respondRedirect("/", permanent = false)
    }
}
