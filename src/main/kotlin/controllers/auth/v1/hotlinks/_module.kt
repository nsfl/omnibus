package com.simfootball.nsfl.controllers.auth.v1.hotlinks

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("hotlinks")
class HotlinksLocation {

    @Location("user/{user}")
    data class UserLocation(val parent: HotlinksLocation, val user: Int)

    @Location("redirect/{payload}")
    data class PayloadLocation(val parent: HotlinksLocation, val payload: String)
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.hotlinksModule() {
    scoped { PayloadLocationResponder() } bind Responder::class
    scoped { UserLocationResponder() } bind Responder::class
}
