package com.simfootball.nsfl.controllers.auth.v1

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.controllers.Router
import com.simfootball.nsfl.controllers.auth.v1.hotlinks.hotlinksModule
import com.simfootball.nsfl.util.Loggable
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.util.KtorExperimentalAPI
import org.koin.core.KoinComponent
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.ext.scope

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
fun Module.authV1Module() {
    single { AuthV1Router() } bind Router::class
    scope<AuthV1Router> {
        hotlinksModule()
    }
}

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class AuthV1Router : Router(), KoinComponent, Loggable {
    override val path: String = "auth/v1"
    override val allResponders: List<Responder<Any>> = scope.getAll()
}
