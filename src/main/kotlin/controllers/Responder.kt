package com.simfootball.nsfl.controllers

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpMethod
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.httpMethod
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.gitlab.llamos.ktorsentry.sentry

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
abstract class Responder<LocType : Any> {

    abstract val locKlass: KClass<LocType>

    suspend fun handle(context: PipelineContext<Unit, ApplicationCall>, loc: LocType) {
        context.sentry.context.addExtra("location", loc)

        when (context.call.request.httpMethod) {
            HttpMethod.Get -> context.get(loc)
            HttpMethod.Post -> context.post(loc)
            HttpMethod.Put -> context.put(loc)
            HttpMethod.Patch -> context.patch(loc)
            HttpMethod.Delete -> context.delete(loc)
        }
    }

    open suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: LocType) {
        throw UnsupportedOperationException()
    }

    open suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: LocType) {
        throw UnsupportedOperationException()
    }

    open suspend fun PipelineContext<Unit, ApplicationCall>.put(loc: LocType) {
        throw UnsupportedOperationException()
    }

    open suspend fun PipelineContext<Unit, ApplicationCall>.patch(loc: LocType) {
        throw UnsupportedOperationException()
    }

    open suspend fun PipelineContext<Unit, ApplicationCall>.delete(loc: LocType) {
        throw UnsupportedOperationException()
    }
}
