package com.simfootball.nsfl.controllers

import com.simfootball.nsfl.util.Loggable
import com.simfootball.nsfl.views.omnibusReactPage
import io.ktor.application.call
import io.ktor.html.respondHtml
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.route
import io.ktor.util.KtorExperimentalAPI
import org.koin.core.KoinComponent

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
class RouterCore : KoinComponent, Loggable {

    private val routers: List<Router> = getKoin().getAll()

    fun invoke(router: Route) = router {
        static("/static") {
            resources("static")
        }

        get("/") {
            call.respondHtml {
                omnibusReactPage(entrypoint = "main")
            }
        }

        routers.forEach {
            route(it.path) { it.invoke(this) }
        }
    }
}
