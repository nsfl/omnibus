package com.simfootball.nsfl.controllers

import com.simfootball.nsfl.controllers.api.v1.apiV1Module
import com.simfootball.nsfl.controllers.auth.v1.authV1Module
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.module

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
val ControllerModule = module {
    apiV1Module()
    authV1Module()
    single { RouterCore() }
}
