package com.simfootball.nsfl.controllers

import com.simfootball.nsfl.util.Loggable
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.handle
import io.ktor.locations.location
import io.ktor.routing.Route
import io.ktor.util.KtorExperimentalAPI
import kotlin.reflect.full.findAnnotation

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
abstract class Router : Loggable {

    abstract val path: String
    abstract val allResponders: List<Responder<Any>>

    fun invoke(route: Route) = route {

        logger.info("Loaded ${allResponders.size} responders")

        for (responder in allResponders) {
            registerResponder(responder)
        }
    }

    private inline fun <reified T : Any> Route.registerResponder(responder: Responder<T>) {
        logger.debug("Registering ${responder::class.simpleName} for location ${responder.locKlass.simpleName}(${responder.locKlass.findAnnotation<Location>()!!.path})")
        location(responder.locKlass) {
            handle(responder.locKlass) { responder.handle(this, it) }
        }
    }
}
