package com.simfootball.nsfl.controllers.api.v1.team

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Teams
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class TeamResponder : Responder<TeamsLocation.TeamLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<TeamsLocation.TeamLocation> = TeamsLocation.TeamLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: TeamsLocation.TeamLocation) {
        logger.debug("Received show-team request for ID ${loc.id}")
        val team = Teams.findById(loc.id, database) ?: throw UnsupportedOperationException()
        call.respond(team)
    }
}
