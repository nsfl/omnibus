package com.simfootball.nsfl.controllers.api.v1.team

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("/teams")
class TeamsLocation() {

    @Location("/{id}")
    data class TeamLocation(val parent: TeamsLocation, val id: Int) {

        @Location("/players")
        data class TeamPlayersLocation(val parent: TeamLocation)
    }
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.teamModule() {
    scoped { TeamResponder() } bind Responder::class
    scoped { TeamPlayersResponder() } bind Responder::class
    scoped { TeamsResponder() } bind Responder::class
}
