package com.simfootball.nsfl.controllers.api.v1.team

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.BankAccount
import com.simfootball.nsfl.model.db.BankAccounts
import com.simfootball.nsfl.model.db.Teams
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.model.rest.requests.CreateTeamRequest
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.database.TransactionIsolation
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class TeamsResponder : Responder<TeamsLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<TeamsLocation> = TeamsLocation::class
    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: TeamsLocation) {
        logger.debug("Received list teams request")
        val teams = database.sequenceOf(Teams).toList()
        call.respond(teams)
    }

    override suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: TeamsLocation) {

        val createRequest = call.receiveOrNull<CreateTeamRequest>() ?: throw IllegalArgumentException()
        val teamName = "${createRequest.city} ${createRequest.mascot}"
        logger.debug("Received create-team request for: $teamName")

        val team = database.useTransaction(TransactionIsolation.SERIALIZABLE) {
            // Create a bank account for the Team
            logger.trace("Creating bank account for new team: $teamName")
            val newAccount = BankAccount { balance = 0 }
            database.sequenceOf(BankAccounts).add(newAccount)
            val accountId = newAccount.id!!
            logger.debug("Created bank account $accountId for new team: $teamName")

            // Create new Team from request and bank account ID and add to DB
            logger.trace("Creating a new team: $teamName with account ID: $accountId")
            val newTeam = createRequest.toTeam(accountId)
            database.sequenceOf(Teams).add(newTeam)
            val teamId = newTeam.id!!
            logger.debug("Created team $teamName with ID: $teamId")

            // this next step collapses the transaction scope
            Teams.findById(teamId, database)
        } ?: throw APIException("Team couldn't be added to DB")

        call.respond(team)
    }
}
