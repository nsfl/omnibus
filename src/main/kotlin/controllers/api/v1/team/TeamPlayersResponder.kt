package com.simfootball.nsfl.controllers.api.v1.team

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Teams
import com.simfootball.nsfl.model.db.players
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class TeamPlayersResponder : Responder<TeamsLocation.TeamLocation.TeamPlayersLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<TeamsLocation.TeamLocation.TeamPlayersLocation> =
        TeamsLocation.TeamLocation.TeamPlayersLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: TeamsLocation.TeamLocation.TeamPlayersLocation) {
        val teamId = loc.parent.id
        logger.debug("Received show players request for Team-ID $teamId")
        val team = Teams.findById(teamId, database) ?: throw APIException("Team not found")
        val players = team.players(database)
        call.respond(players)
    }
}
