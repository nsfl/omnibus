package com.simfootball.nsfl.controllers.api.v1.conference

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("/conferences")
class ConferencesLocation() {

    @Location("/{id}")
    data class ConferenceLocation(val parent: ConferencesLocation, val id: Int) {

        @Location("/teams")
        data class ConferenceTeamsLocation(val parent: ConferenceLocation)
    }
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.conferenceModule() {
    scoped { ConferenceResponder() } bind Responder::class
    scoped { ConferenceTeamsResponder() } bind Responder::class
    scoped { ConferencesResponder() } bind Responder::class
}
