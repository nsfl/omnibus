package com.simfootball.nsfl.controllers.api.v1.conference

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Conferences
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.model.rest.requests.CreateConferenceRequest
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class ConferencesResponder : Responder<ConferencesLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<ConferencesLocation> = ConferencesLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: ConferencesLocation) {
        logger.debug("Received list-conferences request")
        val conferences = database.sequenceOf(Conferences).toList()
        call.respond(conferences)
    }

    override suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: ConferencesLocation) {
        val createRequest = call.receiveOrNull<CreateConferenceRequest>() ?: throw IllegalArgumentException()
        logger.debug("Received create-conference request")

        logger.trace("Creating new conference")
        val newConference = createRequest.toConference()
        database.sequenceOf(Conferences).add(newConference)
        val conferenceId = newConference.id ?: throw APIException("Conference couldn't be added to DB")
        logger.trace("Created conference with ID: $conferenceId")

        val conference = Conferences.findById(conferenceId, database)!!
        call.respond(conference)
    }
}
