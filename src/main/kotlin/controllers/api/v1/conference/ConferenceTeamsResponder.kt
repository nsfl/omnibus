package com.simfootball.nsfl.controllers.api.v1.conference

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Conferences
import com.simfootball.nsfl.model.db.teams
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class ConferenceTeamsResponder :
    Responder<ConferencesLocation.ConferenceLocation.ConferenceTeamsLocation>(),
    KoinComponent,
    Loggable {

    override val locKlass: KClass<ConferencesLocation.ConferenceLocation.ConferenceTeamsLocation> =
        ConferencesLocation.ConferenceLocation.ConferenceTeamsLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>
        .get(loc: ConferencesLocation.ConferenceLocation.ConferenceTeamsLocation) {
            val conferenceId = loc.parent.id
            logger.debug("Received show teams request for conference-ID: $conferenceId")
            val conference = Conferences.findById(conferenceId, database)
                ?: throw APIException("Conference $conferenceId not found")
            val teams = conference.teams(database)
            call.respond(teams)
    }
}
