package com.simfootball.nsfl.controllers.api.v1.league

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Leagues
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.model.rest.requests.CreateLeagueRequest
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class LeaguesResponder : Responder<LeaguesLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<LeaguesLocation> = LeaguesLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: LeaguesLocation) {
        logger.debug("Received list-leagues request")
        val leagues = database.sequenceOf(Leagues).toList()
        call.respond(leagues)
    }

    override suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: LeaguesLocation) {
        val createRequest = call.receiveOrNull<CreateLeagueRequest>() ?: throw IllegalArgumentException()
        logger.debug("Received create-league request")

        logger.trace("Creating new league")
        val newLeague = createRequest.toLeague()
        database.sequenceOf(Leagues).add(newLeague)
        val leagueId = newLeague.id ?: throw APIException("League couldn't be added to DB")
        logger.trace("Created league with ID: $leagueId")

        val league = Leagues.findById(leagueId, database)!!
        call.respond(league)
    }
}
