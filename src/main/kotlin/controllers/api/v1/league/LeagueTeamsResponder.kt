package com.simfootball.nsfl.controllers.api.v1.league

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Leagues
import com.simfootball.nsfl.model.db.teams
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class LeagueTeamsResponder :
    Responder<LeaguesLocation.LeagueLocation.LeagueTeamsLocation>(),
    KoinComponent,
    Loggable {

    override val locKlass: KClass<LeaguesLocation.LeagueLocation.LeagueTeamsLocation> =
        LeaguesLocation.LeagueLocation.LeagueTeamsLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>
        .get(loc: LeaguesLocation.LeagueLocation.LeagueTeamsLocation) {
            val leagueId = loc.parent.id
            logger.debug("Received show teams request for league-ID: $leagueId")
            val league = Leagues.findById(leagueId, database) ?: throw APIException("League $leagueId not found")
            val teams = league.teams(database)
            call.respond(teams)
    }
}
