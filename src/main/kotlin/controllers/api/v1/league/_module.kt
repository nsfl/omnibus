package com.simfootball.nsfl.controllers.api.v1.league

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("/leagues")
class LeaguesLocation() {

    @Location("/{id}")
    data class LeagueLocation(val parent: LeaguesLocation, val id: Int) {

        @Location("/teams")
        data class LeagueTeamsLocation(val parent: LeagueLocation)
    }
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.leagueModule() {
    scoped { LeagueResponder() } bind Responder::class
    scoped { LeagueTeamsResponder() } bind Responder::class
    scoped { LeaguesResponder() } bind Responder::class
}
