package com.simfootball.nsfl.controllers.api.v1.bank

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.BankAccounts
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class BankAccountResponder : Responder<BankAccountsLocation.BankAccountLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<BankAccountsLocation.BankAccountLocation> = BankAccountsLocation.BankAccountLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: BankAccountsLocation.BankAccountLocation) {
        logger.debug("Received show-bank-account request for id ${loc.id}")
        val account = BankAccounts.findById(loc.id, database) ?: throw APIException("Bank account ${loc.id} not found")
        call.respond(account)
    }
}
