package com.simfootball.nsfl.controllers.api.v1.bank

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("/bank-accounts")
class BankAccountsLocation() {

    @Location("/{id}")
    data class BankAccountLocation(val parent: BankAccountsLocation, val id: Int)
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.bankModule() {
    scoped { BankAccountsResponder() } bind Responder::class
    scoped { BankAccountResponder() } bind Responder::class
}
