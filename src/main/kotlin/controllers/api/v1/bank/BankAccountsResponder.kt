package com.simfootball.nsfl.controllers.api.v1.bank

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.BankAccount
import com.simfootball.nsfl.model.db.BankAccounts
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class BankAccountsResponder : Responder<BankAccountsLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<BankAccountsLocation> = BankAccountsLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: BankAccountsLocation) {
        logger.debug("Received list-bank-accounts request")
        val accounts = database.sequenceOf(BankAccounts).toList()
        call.respond(accounts)
    }

    override suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: BankAccountsLocation) {
        logger.debug("Received create-bank-account request")

        logger.trace("Creating new bank account")
        val newAccount = BankAccount { balance = 0 }
        database.sequenceOf(BankAccounts).add(newAccount)
        val accountId = newAccount.id ?: throw APIException("Account couldn't be added to DB")
        logger.trace("Created bank account $accountId")

        val account = BankAccounts.findById(accountId, database)!!
        call.respond(account)
    }
}
