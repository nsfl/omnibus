package com.simfootball.nsfl.controllers.api.v1

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.controllers.Router
import com.simfootball.nsfl.controllers.api.v1.archetypes.archetypeModule
import com.simfootball.nsfl.controllers.api.v1.bank.bankModule
import com.simfootball.nsfl.controllers.api.v1.conference.conferenceModule
import com.simfootball.nsfl.controllers.api.v1.league.leagueModule
import com.simfootball.nsfl.controllers.api.v1.player.playerModule
import com.simfootball.nsfl.controllers.api.v1.team.teamModule
import com.simfootball.nsfl.controllers.api.v1.user.userModule
import com.simfootball.nsfl.util.Loggable
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.util.KtorExperimentalAPI
import org.koin.core.KoinComponent
import org.koin.core.module.Module
import org.koin.dsl.bind
import org.koin.ext.scope

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
fun Module.apiV1Module() {
    single { APIV1Router() } bind Router::class
    scope<APIV1Router> {
        archetypeModule()
        bankModule()
        conferenceModule()
        leagueModule()
        playerModule()
        teamModule()
        userModule()
    }
}

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class APIV1Router : Router(), KoinComponent, Loggable {
    override val path: String = "api/v1"
    override val allResponders: List<Responder<Any>> = scope.getAll()
}
