package com.simfootball.nsfl.controllers.api.v1.user

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Users
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class UserResponder : Responder<UsersLocation.UserLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<UsersLocation.UserLocation> = UsersLocation.UserLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: UsersLocation.UserLocation) {
        logger.debug("Received show-user request for id ${loc.id}")
        val user = Users.findById(loc.id, database) ?: throw APIException("User ${loc.id} not found")
        call.respond(user)
    }
}
