package com.simfootball.nsfl.controllers.api.v1.user

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("/users")
class UsersLocation() {

    @Location("/{id}")
    data class UserLocation(val parent: UsersLocation, val id: Int) {

        @Location("/players")
        data class UserPlayersLocation(val parent: UserLocation)
    }

    @Location("/me")
    data class MeLocation(val parent: UsersLocation)
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.userModule() {
    scoped { UserResponder() } bind Responder::class
    scoped { UserPlayersResponder() } bind Responder::class
    scoped { UsersResponder() } bind Responder::class
    scoped { MeResponder() } bind Responder::class
}
