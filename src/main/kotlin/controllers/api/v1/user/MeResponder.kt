package com.simfootball.nsfl.controllers.api.v1.user

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.services.auth.AuthorizationService
import com.simfootball.nsfl.services.user
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class MeResponder : Responder<UsersLocation.MeLocation>(), KoinComponent, Loggable {
    @KtorExperimentalLocationsAPI
    override val locKlass: KClass<UsersLocation.MeLocation> = UsersLocation.MeLocation::class

    private val authService: AuthorizationService by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: UsersLocation.MeLocation) {
        authService.requireAuth(this)

        // requireAuth ensures we are logged in, so null-assertion is fine
        call.respond(user!!)
    }
}
