package com.simfootball.nsfl.controllers.api.v1.user

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.BankAccount
import com.simfootball.nsfl.model.db.BankAccounts
import com.simfootball.nsfl.model.db.Users
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.model.rest.requests.CreateUserRequest
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.database.TransactionIsolation
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class UsersResponder : Responder<UsersLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<UsersLocation> = UsersLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: UsersLocation) {
        logger.debug("Received list-users request")
        val users = database.sequenceOf(Users).toList()
        call.respond(users)
    }

    override suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: UsersLocation) {
        val createRequest = call.receiveOrNull<CreateUserRequest>() ?: throw IllegalArgumentException()
        logger.debug("Received create-user request for username ${createRequest.username}")

        val user = database.useTransaction(TransactionIsolation.SERIALIZABLE) {
            // Create a bank account for the User
            logger.trace("Creating bank account for new user ${createRequest.username}")
            val newAccount = BankAccount { balance = 0 }
            database.sequenceOf(BankAccounts).add(newAccount)
            val accountId = newAccount.id!!
            logger.debug("Created bank account $accountId for new user ${createRequest.username}")

            // Create new User from request and bank account ID and add to DB
            logger.trace("Creating user account for new user ${createRequest.username}")
            val newUser = createRequest.toUser(accountId)
            database.sequenceOf(Users).add(newUser)
            val userId = newUser.id!!
            logger.debug("Created user account $userId for new user ${createRequest.username}")

            // this next step collapses the transaction scope
            Users.findById(userId, database)
        } ?: throw APIException("User couldn't be added to DB")

        call.respond(user)
    }
}
