package com.simfootball.nsfl.controllers.api.v1.player

import com.simfootball.nsfl.ReqContext
import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.controllers.fallback.responders.BadRequestException
import com.simfootball.nsfl.model.db.Archetypes
import com.simfootball.nsfl.model.db.PlayerHistories
import com.simfootball.nsfl.model.db.PlayerHistory
import com.simfootball.nsfl.model.db.Players
import com.simfootball.nsfl.model.rest.requests.player.EditPlayerRequest
import com.simfootball.nsfl.services.auth.AuthorizationService
import com.simfootball.nsfl.services.auth.AuthorizationService.Companion.AdminScope
import com.simfootball.nsfl.services.player.validation.ArchetypeService
import com.simfootball.nsfl.services.player.validation.PlayerTPECalculatorService
import com.simfootball.nsfl.services.user
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import java.time.Instant
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.database.TransactionIsolation
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class PlayerResponder : Responder<PlayersLocation.PlayerLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<PlayersLocation.PlayerLocation> = PlayersLocation.PlayerLocation::class

    private val database: Database by inject()
    private val archetypeService: ArchetypeService by inject()
    private val authorizationService: AuthorizationService by inject()
    private val tpeCostService: PlayerTPECalculatorService by inject()

    override suspend fun ReqContext.get(loc: PlayersLocation.PlayerLocation) {
        logger.debug("Received show-player request for id ${loc.id}")
        val player = Players.findById(loc.id, database) ?: throw UnsupportedOperationException()
        call.respond(player)
    }

    override suspend fun ReqContext.patch(loc: PlayersLocation.PlayerLocation) {
        logger.debug("Received edit-player request for id ${loc.id}")
        authorizationService.requireAuth(this)
        val player = Players.findById(loc.id, database) ?: throw NoSuchElementException()
        val request = call.receiveOrNull<EditPlayerRequest>() ?: throw BadRequestException()

        database.useTransaction(TransactionIsolation.SERIALIZABLE) {
            // skip pulling archetype if it hasn't changed
            val newArchetype =
                if (request.archetypeId == null || player.archetypeId == request.archetypeId)
                    player.archetype
                else
                    Archetypes.findById(request.archetypeId, database) ?: throw BadRequestException()

            // TODO change auth requirements based on fields edited
            val changedFields = request.applyToPlayer(player)
            if (request.reason == null && !authorizationService.checkAuth(this, AdminScope))
                throw BadRequestException("Must provide a reason")
            if (changedFields.isEmpty())
                throw BadRequestException("No properties changed")

            // check against archetype
            val errors = archetypeService.checkAllAttributes(player, newArchetype)
            if (errors != null) {
                player.discardChanges()
                throw BadRequestException(*errors.toTypedArray())
            }

            // check cost
            val totalTpe = tpeCostService.getTotalTPESpent(player, newArchetype)
            if ((request.tpeSpent ?: player.tpeSpent) != totalTpe) {
                player.discardChanges()
                throw BadRequestException("TPE spent mismatch, got ${request.tpeSpent} should be $totalTpe")
            }

            // track history revision
            val revision = PlayerHistory {
                playerId = player.id!!
                revisionTimestamp = Instant.now()
                changes = changedFields.asSequence().joinToString(",") { "${it.key.name}=${it.value}" }
                authorId = user!!.id
                reason = request.reason
            }
            database.sequenceOf(PlayerHistories).add(revision)
            player.flushChanges()
        }

        // refresh object and return
        val updatedPlayer = Players.findById(player.id!!, database)!!
        return call.respond(updatedPlayer)
    }
}
