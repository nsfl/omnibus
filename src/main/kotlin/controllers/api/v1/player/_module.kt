package com.simfootball.nsfl.controllers.api.v1.player

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("/players")
class PlayersLocation() {

    @Location("/{id}")
    data class PlayerLocation(val parent: PlayersLocation, val id: Int)
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.playerModule() {
    scoped { PlayerResponder() } bind Responder::class
    scoped { PlayersResponder() } bind Responder::class
}
