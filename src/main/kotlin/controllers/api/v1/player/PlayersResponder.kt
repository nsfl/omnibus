package com.simfootball.nsfl.controllers.api.v1.player

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Archetypes
import com.simfootball.nsfl.model.db.Players
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.model.rest.requests.CreatePlayerRequest
import com.simfootball.nsfl.services.player.validation.ArchetypeService
import com.simfootball.nsfl.services.user
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.entity.add
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class PlayersResponder : Responder<PlayersLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<PlayersLocation> = PlayersLocation::class

    private val database: Database by inject()
    private val archetypeService: ArchetypeService by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: PlayersLocation) {
        logger.debug("Received list players request")
        val players = database.sequenceOf(Players).toList()
        call.respond(players)
    }

    override suspend fun PipelineContext<Unit, ApplicationCall>.post(loc: PlayersLocation) {
        val createRequest = call.receiveOrNull<CreatePlayerRequest>() ?: throw IllegalArgumentException()
        val newPlayer = createRequest.toPlayer(user!!)

        // Check that the provided player attributes satisfy the archetype
        val archetype = Archetypes.findById(newPlayer.archetypeId, database) ?: throw IllegalArgumentException()
        val checkResult = archetypeService.checkAllAttributes(newPlayer, archetype)

        if (checkResult != null && checkResult.isNotEmpty()) { // If check fails, we respond with the list of errors
            throw APIException(*checkResult.toTypedArray())
        } else { // If check is successful, we add player to DB
            database.sequenceOf(Players).add(newPlayer)
            val playerId = newPlayer.id ?: throw APIException("Player couldn't be added to DB")
            val player = Players.findById(playerId, database) ?: throw IllegalArgumentException()
            call.respond(player)
        }
    }
}
