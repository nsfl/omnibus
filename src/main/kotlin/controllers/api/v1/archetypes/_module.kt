package com.simfootball.nsfl.controllers.api.v1.archetypes

import com.simfootball.nsfl.controllers.Responder
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.ScopeDSL
import org.koin.dsl.bind

@KtorExperimentalLocationsAPI
@Location("/archetypes")
class ArchetypesLocation() {

    @Location("/{id}")
    data class ArchetypeLocation(val parent: ArchetypesLocation, val id: Int)
}

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
fun ScopeDSL.archetypeModule() {
    scoped { ArchetypesResponder() } bind Responder::class
    scoped { ArchetypeResponder() } bind Responder::class
}
