package com.simfootball.nsfl.controllers.api.v1.archetypes

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Archetypes
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.entity.sequenceOf
import me.liuwj.ktorm.entity.toList
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class ArchetypesResponder : Responder<ArchetypesLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<ArchetypesLocation> = ArchetypesLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: ArchetypesLocation) {
        logger.debug("Received list-archetypes request")

        val archetypes = database.sequenceOf(Archetypes).toList()
        call.respond(archetypes)
    }
}
