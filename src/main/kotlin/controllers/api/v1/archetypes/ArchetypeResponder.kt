package com.simfootball.nsfl.controllers.api.v1.archetypes

import com.simfootball.nsfl.controllers.Responder
import com.simfootball.nsfl.model.db.Archetypes
import com.simfootball.nsfl.model.errors.APIException
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respond
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import kotlin.reflect.KClass
import me.liuwj.ktorm.database.Database
import org.koin.core.KoinComponent
import org.koin.core.inject

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
class ArchetypeResponder : Responder<ArchetypesLocation.ArchetypeLocation>(), KoinComponent, Loggable {

    override val locKlass: KClass<ArchetypesLocation.ArchetypeLocation> = ArchetypesLocation.ArchetypeLocation::class

    private val database: Database by inject()

    override suspend fun PipelineContext<Unit, ApplicationCall>.get(loc: ArchetypesLocation.ArchetypeLocation) {
        logger.debug("Received show-archetype request for id ${loc.id}")

        val archetype = Archetypes.findById(loc.id, database) ?: throw APIException("Archetype ${loc.id} not found")
        call.respond(archetype)
    }
}
