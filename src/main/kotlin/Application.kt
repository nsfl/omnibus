package com.simfootball.nsfl

import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.simfootball.nsfl.controllers.ControllerModule
import com.simfootball.nsfl.controllers.RouterCore
import com.simfootball.nsfl.controllers.fallback.FallbackRouterCore
import com.simfootball.nsfl.services.OmnibusAuthService
import com.simfootball.nsfl.services.auth.AuthenticationServiceModule
import com.simfootball.nsfl.services.db.DatabaseModule
import com.simfootball.nsfl.services.omnibusAuthCookie
import com.simfootball.nsfl.services.player.validation.PlayerValidationModule
import com.simfootball.nsfl.util.loadKtorProperties
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.AutoHeadResponse
import io.ktor.features.CachingHeaders
import io.ktor.features.CallLogging
import io.ktor.features.Compression
import io.ktor.features.ConditionalHeaders
import io.ktor.features.ContentNegotiation
import io.ktor.features.DataConversion
import io.ktor.features.DefaultHeaders
import io.ktor.features.ForwardedHeaderSupport
import io.ktor.features.StatusPages
import io.ktor.features.XForwardedHeaderSupport
import io.ktor.features.deflate
import io.ktor.features.gzip
import io.ktor.features.minimumSize
import io.ktor.http.CacheControl
import io.ktor.http.ContentType
import io.ktor.http.content.CachingOptions
import io.ktor.jackson.jackson
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.request.path
import io.ktor.routing.routing
import io.ktor.sessions.Sessions
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.date.GMTDate
import io.ktor.util.pipeline.PipelineContext
import me.gitlab.llamos.ktorsentry.KtorSentry
import me.liuwj.ktorm.jackson.KtormModule
import org.koin.ktor.ext.Koin
import org.koin.ktor.ext.inject
import org.koin.logger.slf4jLogger
import org.slf4j.event.Level

typealias ReqContext = PipelineContext<Unit, ApplicationCall>

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
@Suppress("unused", "unused_parameter") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    install(Koin) {
        slf4jLogger(org.koin.core.logger.Level.DEBUG)

        loadKtorProperties(environment.config)

        modules(
            AuthenticationServiceModule,
            ControllerModule,
            DatabaseModule,
            PlayerValidationModule
        )
    }

    install(KtorSentry)

    install(Locations) {
    }

    val secret = environment.config.property("omnibus.secret").getString()
    val omnibusEnv = environment.config.property("omnibus.environment").getString()
    if (secret == "changeme" && omnibusEnv != "dev")
        throw RuntimeException("Using the default application secret in a non-development environment is not allowed! You must change the application secret!")

    install(Sessions) {
        omnibusAuthCookie(secret)
    }

    install(OmnibusAuthService)

    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(AutoHeadResponse)

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(ConditionalHeaders)

    install(CachingHeaders) {
        options { outgoingContent ->
            when (outgoingContent.contentType?.withoutParameters()) {
                ContentType.Text.CSS -> CachingOptions(CacheControl.MaxAge(maxAgeSeconds = 24 * 60 * 60), expires = null as? GMTDate?)
                else -> null
            }
        }
    }

    install(DataConversion)

    install(DefaultHeaders) {
        header("X-Engine", "Ktor") // will send this header with each response
    }

    install(ForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy
    install(XForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
            disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
            registerKotlinModule()
            registerModule(KtormModule())
            registerModule(JavaTimeModule())
        }
    }

    install(StatusPages) {
        exception<Exception> { cause ->
            FallbackRouterCore.handleException(this, cause)
        }
    }

    // default response handling with 404
    intercept(ApplicationCallPipeline.Fallback) {
        if (call.response.status() == null) {
            FallbackRouterCore.handleEmptyResponse(this)
        }
    }

    val routerCore: RouterCore by inject()
    routing { routerCore.invoke(this) }
}
