package com.simfootball.nsfl.services

import com.simfootball.nsfl.model.db.User
import com.simfootball.nsfl.model.db.Users
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationFeature
import io.ktor.application.application
import io.ktor.application.call
import io.ktor.application.feature
import io.ktor.application.featureOrNull
import io.ktor.sessions.SessionTransportTransformerMessageAuthentication
import io.ktor.sessions.Sessions
import io.ktor.sessions.clear
import io.ktor.sessions.cookie
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import io.ktor.util.AttributeKey
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import me.gitlab.llamos.ktorsentry.sentry
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.dsl.and
import me.liuwj.ktorm.dsl.eq
import me.liuwj.ktorm.entity.find
import me.liuwj.ktorm.entity.sequenceOf
import org.koin.ktor.ext.get

@Suppress("unused_parameter")
class OmnibusAuthService(config: Configuration, private val database: Database) {

    class Configuration

    companion object Feature : ApplicationFeature<Application, Configuration, OmnibusAuthService> {
        override val key: AttributeKey<OmnibusAuthService> = AttributeKey(OmnibusAuthService::class.simpleName!!)

        @KtorExperimentalAPI
        override fun install(pipeline: Application, configure: Configuration.() -> Unit): OmnibusAuthService {
            val config = Configuration().apply(configure)
            val feature = OmnibusAuthService(config, pipeline.get())

            // ensure that sessions is installed
            val sessions = pipeline.featureOrNull(Sessions)
                ?: throw IllegalStateException("The Sessions pipeline feature must be installed before the OmnibusAuthService")

            // ensure the OAS cookie is registered
            if (sessions.providers.none { it.name == OmnibusAuthService::class.simpleName!! })
                throw IllegalStateException("The omnibusAuthCookie must be registered within the Sessions pipeline feature")

            return feature
        }
    }

    @KtorExperimentalAPI
    fun getUser(context: PipelineContext<Unit, ApplicationCall>): User? = context.run {
        application.featureOrNull(OmnibusAuthService)
            ?: throw IllegalStateException("The OAS feature must be installed to access the current user.")

        // ensure we have a session token
        val session = call.sessions.get<UserSession>()
            ?: return@run null

        // find the user associated with the session
        val user = database.sequenceOf(Users)
            .find { (it.id eq session.userId) and (it.username eq session.username) }

        // if the session exists, but the user doesn't, then the cookie is broken
        if (user == null) {
            call.sessions.clear<UserSession>()
            return@run null
        }

        // place the user in both the call attributes for later access and the sentry context
        call.attributes.put(userAttributeKey, user)
        sentry.context.user = io.sentry.event.User(user.id!!.toString(), user.username, null, null)

        return@run user
    }

    @KtorExperimentalAPI
    fun setUser(context: PipelineContext<Unit, ApplicationCall>, newValue: User?) = context.run {
        application.featureOrNull(OmnibusAuthService)
            ?: throw IllegalStateException("The OAS feature must be installed to access the current user.")

        // clear the cookie if they're signing out
        if (newValue == null) {
            call.sessions.clear<UserSession>()
            call.attributes.remove(userAttributeKey)
            sentry.context.user = null
            return@run
        }

        // ensure the user is a permanent entity in the db
        val userId = newValue.id
            ?: throw IllegalArgumentException("The user must be persisted in the database before applying to the session cookie.")

        // recreate the session token with the new value
        val session = UserSession(userId, newValue.username)
        call.sessions.set(session)
        sentry.context.user = io.sentry.event.User(newValue.id!!.toString(), newValue.username, null, null)
    }
}

data class UserSession(val userId: Int, val username: String)
private val userAttributeKey: AttributeKey<User> = AttributeKey("")

/**
 * Registers the OmnibusAuthService cookie to the Sessions instance.
 * This is required in order to use the OmnibusAuthService.
 */
fun Sessions.Configuration.omnibusAuthCookie(secretKey: String) {
    cookie<UserSession>(OmnibusAuthService::class.simpleName!!) {
        cookie.httpOnly = false
        transform(SessionTransportTransformerMessageAuthentication(secretKey.toByteArray()))
    }
}

/**
 * Returns the user that initiated the request, if any.
 */
@KtorExperimentalAPI
var PipelineContext<Unit, ApplicationCall>.user: User?
    get() = application.feature(OmnibusAuthService).getUser(this)
    set(value) = application.feature(OmnibusAuthService).setUser(this, value)

class AuthenticationException : RuntimeException()
class AuthorizationException : RuntimeException()
