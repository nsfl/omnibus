package com.simfootball.nsfl.services.player.validation

import com.simfootball.nsfl.model.db.Archetype
import com.simfootball.nsfl.model.db.Player
import kotlin.reflect.KProperty1

enum class PlayerArchetypeAttributeMatch(
    val playerAttribute: KProperty1<Player, Int>,
    val lowerBound: KProperty1<Archetype, Int>,
    val upperBound: KProperty1<Archetype, Int>,
    val costsTpe: Boolean = true
) {
    HEIGHT(Player::height, Archetype::minHeight, Archetype::maxHeight, costsTpe = false),
    WEIGHT(Player::weight, Archetype::minWeight, Archetype::maxWeight, costsTpe = false),
    STRENGTH(Player::strength, Archetype::baseStrength, Archetype::maxStrength),
    AGILITY(Player::agility, Archetype::baseAgility, Archetype::maxAgility),
    ARM(Player::arm, Archetype::baseArm, Archetype::maxArm),
    SPEED(Player::speed, Archetype::baseSpeed, Archetype::maxSpeed),
    HANDS(Player::hands, Archetype::baseHands, Archetype::maxHands),
    INTELLIGENCE(Player::intelligence, Archetype::baseIntelligence, Archetype::maxIntelligence),
    ACCURACY(Player::accuracy, Archetype::baseAccuracy, Archetype::maxAccuracy),
    PASS_BLOCKING(Player::passBlocking, Archetype::basePassBlocking, Archetype::maxPassBlocking),
    RUN_BLOCKING(Player::runBlocking, Archetype::baseRunBlocking, Archetype::maxRunBlocking),
    TACKLING(Player::tackling, Archetype::baseTackling, Archetype::maxTackling),
    KICK_DISTANCE(Player::kickDistance, Archetype::baseKickDistance, Archetype::maxKickDistance),
    KICK_ACCURACY(Player::kickAccuracy, Archetype::baseKickAccuracy, Archetype::maxKickAccuracy),
    ENDURANCE(Player::endurance, Archetype::baseEndurance, Archetype::maxEndurance)
}
