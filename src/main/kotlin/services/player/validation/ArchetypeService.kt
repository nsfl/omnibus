package com.simfootball.nsfl.services.player.validation

import com.simfootball.nsfl.model.db.Archetype
import com.simfootball.nsfl.model.db.Player
import com.simfootball.nsfl.util.Loggable
import org.koin.core.KoinComponent

class ArchetypeService : KoinComponent, Loggable {

    fun checkAllAttributes(player: Player, archetype: Archetype): List<String>? {

        val errors = mutableListOf<String>()

        PlayerArchetypeAttributeMatch.values().forEach { att ->
            if (att.playerAttribute.get(player) !in (att.lowerBound.get(archetype)..att.upperBound.get(archetype)))
                errors.add(att.playerAttribute.name.capitalize())
        }

        return if (errors.size > 0) errors else null
    }
}
