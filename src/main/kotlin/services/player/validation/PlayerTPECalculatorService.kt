package com.simfootball.nsfl.services.player.validation

import com.simfootball.nsfl.model.db.Archetype
import com.simfootball.nsfl.model.db.Player
import com.simfootball.nsfl.util.Loggable
import org.koin.core.KoinComponent

class PlayerTPECalculatorService : KoinComponent, Loggable {

    data class TPEThreshold(val threshold: Int, val tpeCost: Int)

    private val thresholds: List<TPEThreshold> = listOf(
        TPEThreshold(90, 15),
        TPEThreshold(80, 10),
        TPEThreshold(70, 5),
        TPEThreshold(50, 2),
        TPEThreshold(0, 1)
    )

    fun getTPECost(attributeValue: Int): Int {
        var myval = attributeValue
        var tpeCost = 0
        thresholds.forEach {
            if (myval > it.threshold) {
                val diff = (myval - it.threshold)
                tpeCost += diff * it.tpeCost
                myval -= diff
            }
        }
        return tpeCost
    }

    fun getUpgradeCost(oldValue: Int, newValue: Int): Int {
        return getTPECost(newValue) - getTPECost(oldValue)
    }

    fun getTotalTPESpent(player: Player, archetype: Archetype): Int {
        var totalTPE = 0
        PlayerArchetypeAttributeMatch.values().forEach { att ->
            if (att.costsTpe)
                totalTPE += getUpgradeCost(att.lowerBound.get(archetype), att.playerAttribute.get(player))
        }
        return totalTPE
    }
}
