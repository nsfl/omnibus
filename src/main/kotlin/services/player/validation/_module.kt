package com.simfootball.nsfl.services.player.validation

import org.koin.dsl.module

val PlayerValidationModule = module {
    single { ArchetypeService() }
    single { PlayerTPECalculatorService() }
}
