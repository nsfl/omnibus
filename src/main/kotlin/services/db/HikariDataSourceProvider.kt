package com.simfootball.nsfl.services.db

import com.simfootball.nsfl.util.Loggable
import com.zaxxer.hikari.HikariDataSource
import javax.sql.DataSource
import org.koin.core.KoinComponent
import org.koin.ext.scope

class HikariDataSourceProvider : DataSourceProvider, KoinComponent, Loggable {

    private val useHikari = getKoin().getProperty("db.hikari", "true").toBoolean()
    private val underlyingProvider: DataSourceProvider by scope.inject()

    override fun createDataSource(dbUrl: String, dbUser: String?, dbPassword: String?): DataSource {
        val underlyingDS = underlyingProvider.createDataSource(dbUrl, dbUser, dbPassword)
        if (!useHikari) {
            logger.warn("Skipping initializing HikariCP for underlying DS ${underlyingDS::class.simpleName}")
            return underlyingDS
        }

        return HikariDataSource().apply {
            dataSource = underlyingDS
        }
    }
}
