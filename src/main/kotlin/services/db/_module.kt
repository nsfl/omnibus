package com.simfootball.nsfl.services.db

import org.koin.dsl.bind
import org.koin.dsl.module

val DatabaseModule = module {
    // getting the db connection
    single { HikariDataSourceProvider() } bind DataSourceProvider::class
    scope<HikariDataSourceProvider> {
        scoped { PGDataSourceProvider() } bind DataSourceProvider::class
    }

    // getting the ktorm instance
    single { KtormDatabaseProvider() }
    factory { get<KtormDatabaseProvider>().db }
}
