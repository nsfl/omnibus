package com.simfootball.nsfl.services.db

import com.simfootball.nsfl.util.Loggable
import javax.sql.DataSource
import me.liuwj.ktorm.database.Database
import me.liuwj.ktorm.logging.Slf4jLoggerAdapter
import org.flywaydb.core.Flyway
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.slf4j.LoggerFactory

class KtormDatabaseProvider : KoinComponent, Loggable {

    private val dbUrl: String = getKoin().getProperty("db.url")!!
    private val dbUser: String? = getKoin().getProperty("db.user")
    private val dbPassword: String? = getKoin().getProperty("db.password")
    private val migrate: Boolean = getKoin().getProperty("db.migrate", "false").toBoolean()
    private val dsProvider: DataSourceProvider by inject()

    val db: Database by lazy {
        // create data source from injection
        val ds = dsProvider.createDataSource(dbUrl, dbUser, dbPassword)

        // perform database migrations
        if (migrate) {
            logger.info("Performing database migrations with Flyway")
            flywayMigrate(ds)
        } else {
            logger.warn("""
            | ########## DATABASE MIGRATIONS SKIPPED ########## |
            |    Skipping database migrations may lead to       |
            |    a disagreement between Omnibus and             |
            |    Postgres on the structure of stored data,      |
            |    leading to potential data loss and crashes.    |
            |    Only disable migrations if you are certain     |
            |    that the database is current.                  |
            | ######## DATABASE MIGRATIONS SKIPPED ############ |""")
        }

        // create ktorm instance
        logger.info("Instantiating Ktorm Database instance with Slf4jLoggerAdapter and koin-injected DS (${ds::class.simpleName})")
        val dbLogger = Slf4jLoggerAdapter(LoggerFactory.getLogger(Database::class.java))
        Database.connect(ds, logger = dbLogger)
    }

    private fun flywayMigrate(ds: DataSource) {
        Flyway.configure()
            .dataSource(ds)
            .locations("classpath:migrations")
            .load()
            .migrate()
    }
}
