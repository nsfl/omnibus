package com.simfootball.nsfl.services.db

import javax.sql.DataSource
import org.koin.core.KoinComponent
import org.postgresql.ds.PGSimpleDataSource

class PGDataSourceProvider : DataSourceProvider, KoinComponent {

    override fun createDataSource(dbUrl: String, dbUser: String?, dbPassword: String?): DataSource {
        return PGSimpleDataSource().apply {
            setUrl(dbUrl)
            user = dbUser!!
            password = dbPassword!!
        }
    }
}
