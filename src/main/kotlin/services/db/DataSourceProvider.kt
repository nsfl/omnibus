package com.simfootball.nsfl.services.db

import javax.sql.DataSource

interface DataSourceProvider {

    fun createDataSource(dbUrl: String, dbUser: String?, dbPassword: String?): DataSource
}
