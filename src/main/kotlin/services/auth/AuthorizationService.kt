package com.simfootball.nsfl.services.auth

import com.simfootball.nsfl.ReqContext
import com.simfootball.nsfl.model.db.User
import com.simfootball.nsfl.services.user
import com.simfootball.nsfl.util.Loggable
import io.ktor.application.call
import io.ktor.request.path
import io.ktor.util.AttributeKey
import io.ktor.util.KtorExperimentalAPI
import java.util.UUID
import org.koin.core.KoinComponent

class UnauthorizedException(message: String) : Exception(message)
class UnauthenticatedException : Exception()

data class AuthorizationScope(
    private val scopeCheck: (User) -> Boolean,
    val cacheKey: String? = UUID.randomUUID().toString()
) {
    operator fun invoke(user: User) = scopeCheck(user)
}

@KtorExperimentalAPI
class AuthorizationService : KoinComponent, Loggable {

    companion object {
        val AdminScope = AuthorizationScope({ false }, cacheKey = "AuthorizationScope_AdminScope")
    }

    private val parsedScopesAttKey: AttributeKey<MutableMap<String, Boolean>> =
        AttributeKey(AuthorizationService::class.simpleName + "_parsedScopes")

    private fun checkScope(context: ReqContext, scope: AuthorizationScope): Boolean {
        // get caching map, or make a new one
        val scopesMap = context.call.attributes
            .computeIfAbsent(parsedScopesAttKey) { mutableMapOf() }

        // check for cached result
        val cachedResult = scopesMap[scope.cacheKey]
        if (cachedResult != null)
            return cachedResult

        // parse scope of active user
        val u = context.user ?: return false
        val hasScope = scope(u)

        // store in cache if applicable
        if (scope.cacheKey != null)
            scopesMap[scope.cacheKey] = hasScope

        return hasScope
    }

    /**
     * Checks whether or not the user represented by the current context has auth scopes.
     * May not pre-compute all requested scopes if a failure is detected early.
     * @return true iff the user is authenticated and has all permissions specified
     */
    fun checkAuth(context: ReqContext, vararg requiredScopes: AuthorizationScope): Boolean {
        return (context.user?.isAdmin ?: false) ||
            (requiredScopes.isNotEmpty() && requiredScopes.all { checkScope(context, it) })
    }

    /**
     * Stricter version of checkAuth which throws UnauthorizedException if checkAuth returns false.
     * Can be used in the controllers as `requireAuth(this, scopes)`.
     */
    fun requireAuth(context: ReqContext, vararg requiredScopes: AuthorizationScope) {
        if (context.user == null)
            throw UnauthenticatedException()
        if (!checkAuth(context, *requiredScopes))
            throw UnauthorizedException("Not permitted access to ${context.call.request.path()}")
    }
}
