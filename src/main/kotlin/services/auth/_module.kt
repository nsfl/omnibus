package com.simfootball.nsfl.services.auth

import io.ktor.util.KtorExperimentalAPI
import org.koin.dsl.module

@KtorExperimentalAPI
val AuthenticationServiceModule = module {
    single { UserAuthenticationService() }
    single { AuthorizationService() }
}
