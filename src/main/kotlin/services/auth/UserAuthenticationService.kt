package com.simfootball.nsfl.services.auth

import at.favre.lib.crypto.bcrypt.BCrypt
import com.simfootball.nsfl.model.db.User
import com.simfootball.nsfl.util.Loggable
import org.koin.core.KoinComponent

class UserAuthenticationService : KoinComponent, Loggable {

    private val hasher = BCrypt.with(BCrypt.Version.VERSION_2A)
    private val verifier = BCrypt.verifyer(BCrypt.Version.VERSION_2A)

    private fun hashPassword(pass: String): String {
        return hasher.hashToString(12, pass.toCharArray())
    }

    fun checkPassword(user: User, submittedPassword: String): Boolean {
        return verifier.verifyStrict(
            submittedPassword.toCharArray(),
            user.passwordHash.toCharArray()
        ).verified
    }

    fun updatePassword(user: User, newPassword: String) {
        user.passwordHash = hashPassword(newPassword)
    }
}
