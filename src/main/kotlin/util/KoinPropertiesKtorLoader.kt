package com.simfootball.nsfl.util

import io.ktor.config.ApplicationConfig
import io.ktor.util.KtorExperimentalAPI
import org.koin.core.KoinApplication

/**
 * Loads Ktor properties into the Koin property configuration.
 * Each property needs to be specified explicitly within the method.
 */
@KtorExperimentalAPI
fun KoinApplication.loadKtorProperties(config: ApplicationConfig) {
    properties(mapOf(
        config.loadProperty("db.url"),
        config.loadProperty("db.user"),
        config.loadProperty("db.password"),
        config.loadProperty("db.migrate"),
        config.loadProperty("db.hikari")
    ))
}

@KtorExperimentalAPI
private fun ApplicationConfig.loadProperty(path: String): Pair<String, String> {
    return path to (propertyOrNull(path)?.getString() ?: "null")
}
