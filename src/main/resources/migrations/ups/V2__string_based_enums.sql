-- replaces current int-based enums to use varchar(64)
-- since ktorm uses string-based entity mapping for enums

alter table archetypes
    alter column position type varchar(64),
    alter column position set not null;

alter table bank_account_transactions
    alter column "status" type varchar(64),
    alter column "status" set not null;

alter table players
    alter column position type varchar(64),
    alter column position set not null;

alter table tpe_claims
    alter column "status" type varchar(64),
    alter column "status" set not null;

alter table tpe_tasks
    alter column "status" type varchar(64),
    alter column "status" set not null;
