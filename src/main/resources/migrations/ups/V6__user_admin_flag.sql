alter table "users"
    add column is_admin boolean not null default false,
    drop column roles;

create table user_roles (
    "user" int references "users" (id),
    "role" varchar(30) not null,
    primary key ("user", "role")
);
