-- adding min and max strength to archetype table
-- setting temp default to avoid null related errors
alter table archetypes
    add column "base_strength" integer not null default 0,
    add column "max_strength" integer not null default 0;

--removing the defaults again as only needed during creation
alter table archetypes
    alter column "base_strength" drop default,
    alter column "max_strength" drop default;
