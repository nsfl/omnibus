create table if not exists archetypes
(
    id serial not null primary key,
    position integer not null,
    name varchar(32) not null,
    example_players text not null,
    min_height integer not null,
    max_height integer not null,
    min_weight integer not null,
    max_weight integer not null,
    exp_base integer not null,
    exp_per_year integer not null,
    exp_per_tpe_milestone integer not null,
    exp_tpe_milestone_amount integer not null,
    base_agility integer not null,
    base_arm integer not null,
    base_speed integer not null,
    base_hands integer not null,
    base_intelligence integer not null,
    base_accuracy integer not null,
    base_pass_blocking integer not null,
    base_run_blocking integer not null,
    base_tackling integer not null,
    base_kick_distance integer not null,
    base_kick_accuracy integer not null,
    base_endurance integer not null,
    max_agility integer not null,
    max_arm integer not null,
    max_speed integer not null,
    max_hands integer not null,
    max_intelligence integer not null,
    max_accuracy integer not null,
    max_pass_blocking integer not null,
    max_run_blocking integer not null,
    max_tackling integer not null,
    max_kick_distance integer not null,
    max_kick_accuracy integer not null,
    max_endurance integer not null
);

create table if not exists bank_accounts
(
    id serial not null primary key,
    balance integer not null
);

create table if not exists seasons
(
    id serial not null primary key,
    start_date date not null,
    end_date date not null,
    sim_start_date date not null,
    sim_end_date date not null
);

create table if not exists users
(
    id serial not null primary key,
    username varchar(64) not null,
    password_hash text not null,
    forum_username varchar(64) not null,
    roles text not null,
    bank_account integer not null references bank_accounts on update cascade on delete restrict
);

create table if not exists bank_account_transactions
(
    id serial not null primary key,
    season integer not null references seasons on update cascade on delete restrict,
    recipient integer not null references bank_accounts on update cascade on delete cascade,
    amount integer not null,
    sender integer references bank_accounts on update cascade on delete set null,
    notes text,
    status integer not null,
    responder integer references users on update cascade on delete set null
);

create table if not exists leagues
(
    id serial not null primary key,
    name varchar(64) not null,
    acronym varchar(5) not null
);

create table if not exists conferences
(
    id serial not null primary key,
    name varchar(64) not null,
    acronym varchar(5) not null,
    league integer not null references leagues on update cascade on delete restrict
);

create table if not exists teams
(
    id serial not null primary key,
    city varchar(32) not null,
    mascot varchar(32) not null,
    acronym varchar(5) not null,
    conference integer not null references conferences on update cascade on delete restrict,
    owner integer references users on update cascade on delete restrict,
    bank_account integer not null references bank_accounts on update cascade on delete restrict,
    start_season integer not null references seasons on update cascade on delete restrict,
    end_season integer references seasons on update cascade on delete restrict
);

create table if not exists players
(
    id serial not null primary key,
    "user" integer not null references users on update cascade on delete restrict,
    draft_season integer not null references seasons on update cascade on delete restrict,
    retirement_season integer references seasons on update cascade on delete restrict,
    active boolean not null,
    tpe_spent integer not null,
    tpe_banked integer not null,
    position integer not null,
    archetype integer not null references archetypes on update cascade on delete restrict,
    first_name varchar(32) not null,
    last_name varchar(32) not null,
    team integer references teams on update cascade on delete restrict,
    college varchar(64) not null,
    jersey_number integer not null,
    age integer not null,
    experience integer not null,
    height integer not null,
    weight integer not null,
    strength integer not null,
    agility integer not null,
    arm integer not null,
    speed integer not null,
    hands integer not null,
    intelligence integer not null,
    accuracy integer not null,
    pass_blocking integer not null,
    run_blocking integer not null,
    tackling integer not null,
    kick_distance integer not null,
    kick_accuracy integer not null,
    endurance integer not null
);

create table if not exists player_histories
(
    id serial not null primary key,
    player integer not null references players on update cascade on delete cascade,
    revision_timestamp timestamp not null,
    changes text not null,
    author integer references users on update cascade on delete set null,
    reason text
);

create table if not exists tpe_tasks
(
    id serial not null primary key,
    creator integer references users on update cascade on delete set null,
    name text not null,
    description text not null,
    link text,
    status integer not null,
    claim_link text not null,
    season integer not null references seasons on update cascade on delete restrict
);

create table if not exists tpe_claims
(
    id serial not null primary key,
    claimant integer not null references players on update cascade on delete cascade,
    tpe_task integer references tpe_tasks on update cascade on delete set null,
    link text,
    status integer not null,
    responder integer references users on update cascade on delete set null
);
