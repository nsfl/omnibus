create table "user_auth_hotlinks" (
    target_user int not null references users (id) primary key,
    payload text not null unique
);
