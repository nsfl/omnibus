-- adding offseason start date to seasons
-- setting temp default to avoid null related errors
alter table seasons
    add column "off_season_start_date" date not null default '2020-01-01';

--removing the default again as only needed during creation
alter table seasons
    alter column "off_season_start_date" drop default;
